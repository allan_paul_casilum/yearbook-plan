<form id="page_content" name="page_content" method="post" action="#" enctype="multipart/form-data">
<div class="row">
  <div class="col-sm-12 col-md-8">
    <p><a href="<?php echo site_url(YB_PUBLIC_TASK_LIST);?>">Back to List</a></p>
    <h4><?php echo $post['title']; ?></h4>
    <p>Due Date : <span class="badge badge-<?php echo $standing;?>"><?php echo $post['due_date_standing']; ?> | <?php echo $post['due_date_format']; ?></span></p>
    <p>
      Assigned Article Length : <?php echo $full_block_size; ?>
      <span>
        <?php if ( $part_block_size > 0 ) : ?>
           and <?php echo $part_block_size;?>
        <?php endif; ?>
        page(s)
      </span>
    </p>
    <p>Estimated Article Length :
      <span class="badge badge-<?php echo $assign_article_length_status;?>">
        <?php
          $arr_approximate = yb_extract_float($approximate_page_size);
        ?>
        <?php echo $arr_approximate['whole']; ?> and <?php echo $arr_approximate['fraction']; ?> page(s)
        ( <?php echo $word_count;?> words and <?php echo $image_count;?> images )
      </span>
    </p>
    <!-- <p>Current Word Count : <?php //echo str_word_count(wp_strip_all_tags($post['post_content']));?> words</span></p> -->
    <!--<p>Photo Count : <span class="img-count"><?php //echo count($images); ?></p>-->

      <div class="card">
        <div class="card-body">
          <h3>Instructions</h3>
          <ul>
            <li>Article title – write an article on the title given above.</li>
            <li>Deadline – complete this by the deadline date shown.</li>
            <li>Assigned Article length –word and photo count is appropriate for article length. One page is 800 words. A photo is 75 words.</li>
            <li>Estimated Article Length –  Green indicates article is appropriate length. Red indicates it is not. Press 'save' after adding text and photos to see this.</li>
            <li>To add text – type or copy and paste into box below.</li>
            <li>To upload photos – press the "Choose Files" button, navigate to where the photo/photos are, select the photo/photos and press open.</li>
            <li>To save – press blue ‘save’ You can come back to this article at a later date.</li>
            <li>To finish – Select ‘Yes’ from the drop down and click 'save'.</li>
          </ul>
          <!--<input id="submit_my_page_content" class="btn btn-large btn-primary" name="submit_my_page_content" type="submit" value="Update" />-->
        </div>
      </div>

      <div class="editor">
        <?php wp_editor($post['post_content'], 'pagecontent', ['media_buttons'=>false]); ?>
      </div>
      <?php wp_nonce_field( 'add_page_content_'.$post['id'], 'page-content' ); ?>

      <div class="card">
        <div class="card-body">
          <input id="submit_my_page_content" class="btn btn-large btn-primary" name="submit_my_page_content" type="submit" value="Save" />
        </div>
      </div>
      <input type="hidden" name="post_id" id="post_id" value="<?php echo $post['id'];?>" />

  </div>
  <div class="col-sm-12 col-md-4">
    <div class="card mb-3">
      <div class="card-header">Options</div>
      <div class="card-body">
        <div class="options">
          <p class="card-title">Finished? if this is done please select "Yes"</p>
          <select name="submitted">
            <option value="0" <?php echo ($post['submitted']==0) ? 'selected':'';?>>No</option>
            <option value="1" <?php echo ($post['submitted']==1) ? 'selected':'';?>>Yes</option>
          </select>
          <p></p>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="card-header">
        Add Files
      </div>
      <div class="card-body">
        <div class="upload-image">
          <input type="file" name="files[]" id="files" class="user_picked_files" multiple>
          <span class="upload-image-ajax-status"></span>
          <div class="blueimp-img-list"></div>
        </div>
        <div class="list-images">
          <div class="card-columns">
            <div id="list-images-data">
              <?php if(count($images) > 0) { ?>
                      <?php foreach($images as $k => $v) { ?>
                              <div class="card image-<?php echo $v->ID;?>">

                                <?php if( wp_attachment_is_image($v->ID) ) : ?>
                                  <?php echo wp_get_attachment_image( $v->ID, "thumbnail", "", array( "class" => "img-responsive card-img newsfeed-fit" ) );  ?>
                                <?php else : ?>
                                  <p class="card-text"><?php echo basename( get_attached_file($v->ID) );?></p>
                                <?php endif; ?>
                                <span class="badge badge-danger remove-img-db" data-id="<?php echo $v->ID;?>">Remove</span>

                              </div>
                      <?php } ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
      // $partial_template = YB_View::get_instance()->public_part_partials('partials/dashboard/list-partials.php');
      // YB_View::get_instance()->display($partial_template, $data);
    ?>
  </div>

</div>
</form>
