=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: allteams.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.5.1
Stable tag: 1.12.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

A few notes about the sections above:

*   "Contributors" is a comma separated list of wp.org/wp-plugins.org usernames
*   "Tags" is a comma separated list of tags that apply to the plugin
*   "Requires at least" is the lowest version that the plugin will work on
*   "Tested up to" is the highest version that you've *successfully used to test the plugin*. Note that it might work on
higher versions... this is just the highest one you've verified.
*   Stable tag should indicate the Subversion "tag" of the latest stable version, or "trunk," if you use `/trunk/` for
stable.

    Note that the `readme.txt` of the stable tag is the one that is considered the defining one for the plugin, so
if the `/trunk/readme.txt` file says that the stable tag is `4.3`, then it is `/tags/4.3/readme.txt` that'll be used
for displaying information about the plugin.  In this situation, the only thing considered from the trunk `readme.txt`
is the stable tag pointer.  Thus, if you develop in trunk, you can update the trunk `readme.txt` to reflect changes in
your in-development version, without having that information incorrectly disclosed about the current stable version
that lacks those changes -- as long as the trunk's `readme.txt` points to the correct stable tag.

    If no stable tag is provided, it is assumed that trunk is stable, but you should specify "trunk" if that's where
you put the stable version, in order to eliminate any doubt.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `yearbook-plan.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.12.0 = 
* Add Bulk Delete

= 1.11.0 =
* Add Page to export and download the released and in production articles.
* Add Page to show history of downloaded export in production articles.
* Remove In Production status in the dropdown article list.

= 1.10.0 =
* Add WordUser as contributor

= 1.9.0 =
* Add PDF in attachment content.

= 1.8.0 =
* Add preview page
* Fix Bulkedit
* Custom page size should only be numbers or decimals only.
* Article position should be a number only.

= 1.7.26 =
* Add cursor arrow when moving articles to re-order

= 1.7.25 =
* Update article list and activate the drag and drop menu order
* Improve the re index of the menu order

= 1.7.21 =
* Fixed when yearbook is on draft and updating any articles should not send reminders, unless if its publish then send reminders.
* Fix Bug on updating article title and if its not on-going then it should not send reminders.

= 1.7.20 =
* Fixed when yearbook is set to draft should remove all reminder those on going status only.

= 1.7.19 =
* Update on reminder API and notification flag, remove them if status is set to not "on going".

= 1.7.18 =
* On editing contributor, cannot update email if currently has active notifications.

= 1.7.17 =
* Fix a issue on current user logged in cookie base change to server side.

= 1.7.16 =
* display count images uploaded or added in admin.

= 1.7.15 =
* refactor admin page view

= 1.5.15 =
* Restrict the article on editing in admin page if its on production, only super admin can access the edit page.
* Bulkedit, exclude the articles that are in production.

= 1.5.13 =
* fix export to zip removed apostrophe main title.

= 1.5.12 =
* fix persistent &nbsp; character in export, convert ampersand to symbol.
* update status table tr min width to 150px.

= 1.5.10 =
* Fix bulk update on cover mark articles
* Fix pagination on export

= 1.5.8 =
* Fix bug in updating the yearbook and sending reminders of the pages.
* Update reminder app when removing existing contributors on article.
* Remove email sent notification flag when removing existing contributors on article.

= 1.5.5 =
* Create a reminder API update, apply when a article title is changed,
the idea is to check if the title is different or was changed, if so,
removed the tag in the reminder APP then re-add it on the new title.

= 1.4.5 =
* Added status dropdown in the view/edit page in the backend
* Updated the function creating page articles, status default to publish

= 1.4.3 =
* Add a clear reminder button individually on each articles and in bulk edit.
* Remove reminder when the status is changed except for "on going"

= 1.0 =
* A change since the previous version.
* Another change.

= 0.5 =
* List versions from most recent at top to oldest at bottom.

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
