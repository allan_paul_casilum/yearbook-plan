<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
die;
}
/**
 *
 * @since 0.0.1
 * */
class YB_Page_Preview {
    /**
     * instance of this class
     *
     * @since 0.0.1
     * @access protected
     * @var	null
     * */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     0.0.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
            * - Uncomment following lines if the admin class should only be available for super admins
            */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {
    }

    //get by status
    public function showById( $args = [])
    {
        //meta is_finished
        //0 on going
        //1 author complete
        //5 checked
        //2 proof ready
        //3 ready for production
        //4 in production
        $hasArticle = false;
        $data = [];
        $id = $args['article-id'] ?? false;
        $imagesize = $args['imagesize'] ?? 'large';

        if($id)
        {
            $posts = get_posts([
                'numberposts' => 1,
                'post_status' => 'any',
                'post_type' => YB_CPT_PREFIX,
                'include' => [$id],
                'suppress_filters' => false,
                'meta_key' => 'is_finished',
                'meta_value' => 4
            ]);
            if ( $posts ) {
                $data['posts'] = $posts[0];

                $medias = get_attached_media( '', $id );
                // $attachImages = [];
                // apyc_dd($medias);
                // foreach($medias as $media){
                //     $attachImages[] = wp_get_attachment_image_src($media->ID, $imagesize);
                // }
                // $images = $attachImages;
                $data['attachments'] = $medias;
                $hasArticle = true;
            }
        }

        if($hasArticle)
        {
            YB_View::get_instance()->admin_partials('partials/preview/preview-single-article.php', $data);
        } else {
            yb_redirect_to( yb_admin_url_yearbookpage() );
        }
    }

    //get by status
    public function show( $args = [])
    {
        //meta is_finished
        //0 on going
        //1 author complete
        //5 checked
        //2 proof ready
        //3 ready for production
        //4 in production

        $data = [];
		$id = $args['article-id'] ?? 0;
        $imagesize = $args['imagesize'] ?? 'large';
		if($id !== 0) {
			$arg_parent = [
				'include' => $id,
				'post_type' => YB_CPT_PREFIX,
				'post_status' => 'any',
			];

			$parent_post   = get_posts( $arg_parent );
			$name = $parent_post[0]->post_title;
			$status = $parent_post[0]->post_status;

			$data['status'] = $status;
			$data['name'] = $name;
			$data['action_url'] = yb_admin_url_yearbookpage();
			$data['method'] = 'update';
			$data['plugin_page_title'] = '';
			$fields = yb_verbage('fields');
			$data['verbage_fields'] = $fields;
			$data['is_admin'] = false;

			$current_user = wp_get_current_user();

			if( current_user_can('manage_options') ) {
				$data['is_admin'] = true;
				$account_model = new YB_Account_Model;
				$accounts = $account_model->get();
				$data['accounts'] = $accounts;

				$data['obj_account_meta'] = new YB_Account_Meta;
				$school_admin_id = $parent_post[0]->post_author;
				$data['school_admin_id'] = $school_admin_id;
				$data['is_admin'] = true;
			}else{
				//$current_user = wp_get_current_user();
				$data['current_user'] = $current_user;
				$school_admin_id = get_current_user_id();
				$data['school_admin_id'] = $school_admin_id;
			}
			$data['school_name'] = get_userdata($school_admin_id);
			$data['yearbook_id'] = $id;
			$data['show_controller'] = true;

			if ( get_post_meta($id, 'school_admin_id', true) ) {
				$data['school_admin_id'] = get_post_meta($id, 'school_admin_id', true);
			}
			$data['blocks'] = YB_Articles::get_instance()->get([ 'id' => $id ]);
            //apyc_dd($data['blocks']['blocks']);
            foreach ( $data['blocks']['blocks'] as $key => $val ) {
                if ( $val['is_finished'] != 4 ) {
                    unset($data['blocks']['blocks'][$key]);
                }
                if ( $val['is_finished'] == 4 ) {
                    $medias = get_attached_media( 'image', $val['id'] );
                    $attachImages = [];
                    
                    foreach($medias as $media){
                        $attachImages[] = wp_get_attachment_image_src($media->ID, $imagesize);
                    }
                    $data['blocks']['blocks'][$key]['images'] = $attachImages;
                }
            }
            YB_View::get_instance()->admin_partials('partials/preview/preview.php', $data);
		}else{
			yb_redirect_to( yb_admin_url_yearbookpage() );
		}

    }

}
