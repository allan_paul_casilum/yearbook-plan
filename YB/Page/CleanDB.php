<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * Bulk Edit
 * @since 0.0.1
 * */
class YB_Page_CleanDB {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct()	{

	}

	public function clean_notify( $article_id = null ) {
		global $wpdb;
		$query = "DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = ".$article_id." AND meta_key LIKE 'sent_notify_email_author_id_%'";
		$sql = $wpdb->query($query);
		return $sql;
	}

	public function clean_notify_article( $article_id = null ) {
		global $wpdb;
		$query = "DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = ".$article_id." AND meta_key LIKE 'sent_notify_email_%'";
		$sql = $wpdb->query($query);
		return $sql;
	}

	public function clean_notify_article_user_id( $user_id, $article_id ) {
		global $wpdb;
		$query = "DELETE FROM ".$wpdb->prefix."postmeta WHERE meta_key = 'sent_notify_email_{$user_id}_{$article_id}'";
		$sql = $wpdb->query($query);
		return $sql;
	}

	public function clean_notify_user_id( $user_id, $article_id ) {
		global $wpdb;
		$query = "DELETE FROM ".$wpdb->prefix."postmeta WHERE meta_key = 'sent_notify_email_author_id_{$user_id}'";
		$sql = $wpdb->query($query);
		return $sql;
	}

  public function clean( $args = [] ) {
		global $wpdb;
    $parent_id = 0;
    $school_id = 0;

    if ( isset( $args['post_id'] ) ) {
      $post_id = $args['post_id'];
    }
    if ( isset( $args['school_id'] ) ) {
      $school_id = $args['school_id'];
    }
    if ( isset( $args['parent_id'] ) ) {
      $parent_id = $args['parent_id'];
    }

    if ( $parent_id != 0 ) {
      $args = [
        'post_type' => 'yearbook-plan',
        'post_parent' => $parent_id,
        'posts_per_page' => -1,
				'post_status' => ['publish', 'pending', 'draft' ]
      ];

			$this->clean_notify($parent_id);
			$this->clean_notify_article($parent_id);

      $query = new WP_Query( $args );
      if ( $query->have_posts() ) {
				$arr_post_id = [];
        while ( $query->have_posts() ) {
          $query->the_post();
          $post_id = get_the_ID();
					$post_author = get_the_author_meta('ID');

          $medias = get_attached_media( '', $post_id );
          if ( $medias ) {
            foreach ( $medias as $media ) {
              wp_delete_attachment($media->ID, 1);
            }
          }

          $update_post = array(
            'ID'           => $post_id,
            'post_content'   => '',
          );
          wp_update_post($update_post);

          $due_date = YB_Project_PagesMeta::get_instance()->due_date([
            'action' => 'u',
            'post_id' => $post_id,
            'value' => ''
          ]);
          $status = YB_Project_PagesMeta::get_instance()->status([
            'action' => 'u',
            'post_id' => $post_id,
            'value' => 0
          ]);
          $submitted = YB_Project_PagesMeta::get_instance()->submitted([
            'action' => 'u',
            'post_id' => $post_id,
            'value' => 0
          ]);

					//remove in the reminder api
					$authors = YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
		  			'post_id' => $post_id,
		  			'action' 	=> 'r',
		  			'single' 	=> true
		  		]);
					$res = yb_reminder_api_clear([
						'block_id' 	=> $post_id,
						'author_id' => $authors,
					]);
        }

      }
			wp_reset_postdata();

			$update_parent_post = [
        'ID' => $parent_id,
        'post_status' => 'draft'
      ];

			wp_update_post($update_parent_post);

    }//if

  }//clean method

}
