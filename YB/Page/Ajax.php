<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
* Use to update yearbook through ajax.
* create, delete, update and sort
* @since 0.0.1
* */
class YB_Page_Ajax {
	/**
	* instance of this class
	*
	* @since 0.0.1
	* @access protected
	* @var	null
	* */
	protected static $instance = null;

	/**
	* Return an instance of this class.
	*
	* @since     0.0.1
	*
	* @return    object    A single instance of this class.
	*/
	public static function get_instance() {

		/*
		* - Uncomment following lines if the admin class should only be available for super admins
		*/
		/* if( ! is_super_admin() ) {
		return;
	} */

	// If the single instance hasn't been set, set it now.
	if ( null == self::$instance ) {
		self::$instance = new self;
	}

	return self::$instance;
}

/**
* Create the yearbook.
**/
public function create_yearbook() {
	global $wpdb; // this is how you get access to the database

	$data = [];
	//this will hold the id's of the yearbook section/category
	//save as post meta
	$section_category = [];
	$block_menu = 0;
	$page_number = 1;
	$index = 0;
	//parse the form inputs.
	parse_str($_POST['form'], $form);

	//check if the form is already in DB
	$yearbook_id = isset($form['yearbook_id']) ? $form['yearbook_id'] : false;
	$yearbook_name = isset($form['yearbook-page-name']) ? trim($form['yearbook-page-name']) : false;

	//user owner
	if(current_user_can('manage_options')) {
		$user_id = $form['school_admin'];
	}else{
		$user_id = get_current_user_id();
	}
	//user owner

	//yearbook title
	if(
		!$yearbook_id
		&& $yearbook_id == 0
	){
		if($yearbook_name){
			$yb_name = get_page_by_title($yearbook_name, OBJECT, YB_CPT_PREFIX);
			if($yb_name) {
				$yearbook_id = $yb_name->ID;
			}
		}else{
			$yearbook_name = 'No Title - '.date("Y-m-d-Hi");
		}
	}else{
		$yearbook_name = 'No Title - '.date("Y-m-d-Hi");
	}
	//yearbook title

	//get_page_by_title
	//section category
	$post_status = isset($form['status']) ? $form['status'] : 'draft';
	$new_block = isset($form['new_task']['block']) ? $form['new_task']['block'] : false;
	$existing_tasks = isset($form['task']) ? $form['task'] : false;
	$menu_order = isset($form['menu_order']) ? $form['menu_order'] : 0;

	//get section taxonomy | terms

	wp_defer_term_counting( true );
	wp_defer_comment_counting( true );

	//check if the yearbook id exists
	//if it exists we insert it
	//else update only
	if(!$yearbook_id) {
		//insert
		//parent first
		$insert_parent = [
			'post_title' => $yearbook_name,
			'post_type' => YB_CPT_PREFIX,
			'post_author' => $user_id,
			'post_status' => $post_status,
			'menu_order' => $menu_order,
			'meta_input' => [
				'is_parent_page' => 1,
				'school_admin_id' => $user_id,
			]
		];
		$yearbook_id = wp_insert_post($insert_parent);
	}else{
		//update
		$update_parent = [
			'ID' => $yearbook_id,
			'post_title' => $form['yearbook-page-name'],
		];
		wp_update_post($update_parent);
	}
	//parent

	//block
	//use to re-order
	YB_Article_ReIndexMenuOrder::get_instance()->reIndex([
		'blocks' => $existing_tasks
	]);

	if(
		$new_block
		&& count($new_block) >= 1
	){
		foreach($new_block as $k_block => $v_block){
			//insert
			$block_size = $v_block['block_size_fullpage'] . $v_block['block_size_partpage'];
			if($v_block['block_size_partpage'] == 0){
				$block_size = $v_block['block_size_fullpage'];
			}
			if ( isset($v_block['custom_block_size_fullpage']) && ($v_block['block_size_fullpage'] == '-1' && $v_block['custom_block_size_fullpage'] != 0) ) {
				$block_size = $v_block['custom_block_size_fullpage'];
			}
			$insert_block_child_post = [
				'post_title' => $v_block['block_title'],
				'post_type' => YB_CPT_PREFIX,
				'post_author' => $user_id,
				'post_parent' => $yearbook_id,
				'post_status' => $post_status,
				'menu_order' => $v_block['menu_order'],
				'meta_input' => [
					'block_title' => $v_block['block_title'],
					'template' => $v_block['template'],
					'due_date' => $v_block['due_date'],
					'block_size_fullpage' => $v_block['block_size_fullpage'],
					'block_size_partpage' => $v_block['block_size_partpage'],
					'block_size' => $block_size,
					'submitted' => 0,
					'is_child_block' => 1,
					'is_finished' => 0,
					'school_admin_id' => $user_id,
					'yb_uniqid' => uniqid(),
					'is_cover' => $v_block['is_cover']
				]
			];
			$block_id = wp_insert_post($insert_block_child_post);

			YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
				'post_id' => $block_id,
				'action' => 'u',
				'value' => $v_block['post_author']
			]);
			do_action('yb_after_insert_block', $block_id, $insert_block_child_post, $post_status);
		}

	}

	//block
	//update yearbook main post
	//add the meta section
	//update yearbook main post

	wp_defer_term_counting( false );
	wp_defer_comment_counting( false );

	if($yearbook_id){
		$data['id'] = $yearbook_id;
		$contents = YB_Project_Rest_Task::get_instance()->get_v2($data);
		return wp_send_json($contents);
	}

	wp_die();
}

/**
* Update the yearbook.
**/
public function update_yearbook()
{
	$post_status = isset($_POST['post_status']) ? $_POST['post_status'] : false;
	// Array
	// (
	//   [action] => yb_update_yearbook
	//   [yearbook_id] => 1119
	//   [assign_to] =>
	//   [block_size_fullpage] => 1
	//   [block_size_partpage] => 0
	//   [block_size] => 10
	//   [block_title] => dsad dsd as
	//   [meta_block_title] => dsad dsd as
	//   [meta_block_status] => 0
	//   [is_cover] => 0
	//   [menu_order] => 2
	//   [custom_block_size_fullpage] => 0
	//   [due_date] => 2020-10-07
	//   [post_status] => draft
	//   [parent_id] => 1096
	// )
	if( isset( $_POST['yearbook_id'] ) ) {
		$yearbook_id = $_POST['yearbook_id']; //the block/article id.
		$parent_id = $_POST['parent_id']; //the parent id.

		$post_assign_to = [];
		if( is_array( $_POST['assign_to'] ) && ! empty( $_POST['assign_to'] ) ) {
			$post_assign_to = $_POST['assign_to'];
		}

		$block_title = $_POST['block_title'];
		$post_status = $_POST['post_status'];
		$is_cover = $_POST['is_cover'];
		$due_date = $_POST['due_date'];

		$dbSubmittedMeta = YB_Project_PagesMeta::get_instance()->submitted([
			'post_id' => $yearbook_id,
			'action' => 'r',
			'single' => true
		]);
		// $dbGetIsFinished = YB_Project_PagesMeta::get_instance()->is_finished([
		// 	'post_id' => $yearbook_id,
		// 	'action' => 'r',
		// 	'single' => true
		// ]);
		// $dbSubmittedMeta = 0;
		// if ( $dbGetIsFinished > 0 ) {
		// 	$dbSubmittedMeta = 1;
		// }

		$submitted = isset($_POST['submitted']) ? $_POST['submitted'] : $dbSubmittedMeta;

		$meta_block_status = $_POST['meta_block_status'];
		$meta_block_title = $_POST['meta_block_title'];
		$menu_order = $_POST['menu_order'];

		//put this to function as utils.
		$block_size_fullpage = $_POST['block_size_fullpage'];
		$block_size_partpage = $_POST['block_size_partpage'];
		$custom_block_size_fullpage = $_POST['custom_block_size_fullpage'];

		$block_size = $block_size_fullpage . $block_size_partpage;
		if( $block_size_partpage == 0 ) {
			$block_size = $block_size_fullpage;
		}
		if ( isset($custom_block_size_fullpage) && ($block_size_fullpage == '-1' && $custom_block_size_fullpage != 0) ) {
			if ( $block_size_partpage == 0 ) {
				$block_size_partpage = '';
			}
			$block_size = (int)$custom_block_size_fullpage . $block_size_partpage;
		}
		//put this to function as utils.

		$isPublished = true;
		if ( $post_status == 'draft' ) {
			$isPublished = false;
		}

		if ( $meta_block_status == 0 ) {
			//check if title was changed
			$metaBlockTitle = $meta_block_title;
			$inputBlockTitle = $block_title;
			if ( strcmp($metaBlockTitle, $inputBlockTitle) !== 0 && $isPublished ) {
				YB_ReminderUpdateTag::get_instance()->update($_POST);
			}
		}

		if ( $block_size_fullpage != '-1' ) {
			$custom_block_size_fullpage = 0;
		}

		$getAuthors = yb_get_assign_contributors( $yearbook_id );

		if ( ! $getAuthors ) {
			$getAuthors = [];
		}

		$update_block_child_post = [
			'ID' => $yearbook_id,
			'post_title' => $block_title,
			'post_status' => $post_status,
			'post_author' => $post_assign_to,
			'menu_order' => $menu_order,
			'meta_input' => [
				'block_title' => $block_title,
				'due_date' => $due_date,
				'block_size_fullpage' => $block_size_fullpage,
				'block_size_partpage' => $block_size_partpage,
				'block_custom_size' => $custom_block_size_fullpage,
				'is_cover' => $is_cover,
				'block_size' => $block_size,
				'submitted' => $submitted,
				'is_child_block' => 1,
			]
		];

		wp_update_post($update_block_child_post);

		$authorsRemoved = array_diff( $getAuthors, $post_assign_to );
		if ( count($authorsRemoved) > 0 ) {
			$res = yb_reminder_api_clear([
				'block_id' => $yearbook_id,
				'author_id' => $authorsRemoved,
			]);
			//remove the notification email flag
			yb_clear_notification_flag([
				'article_id' => $yearbook_id,
				'parent_id' => $parent_id,
				'users' => $authorsRemoved
			]);
		} else {
			//remove them all
			if ( empty($post_assign_to) && count($getAuthors) > 0 ) {
				$res = yb_reminder_api_clear([
					'block_id' => $yearbook_id,
					'parent_id' => $parent_id,
					'author_id' => $getAuthors,
				]);
				//remove the notification email flag
				yb_clear_notification_flag([
					'article_id' => $yearbook_id,
					'parent_id' => $parent_id,
					'users' => $getAuthors
				]);
			}
		}

		YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
			'post_id' => $yearbook_id,
			'action' => 'u',
			'value' => $post_assign_to
		]);

		do_action('yb_after_update_yearbook_single_blocks', $update_block_child_post, $post_status, $_POST);
	}

	wp_die();
}

/**
* Sort Articles.
**/
public function sort_blocks()
{
	parse_str($_POST['form'], $form);

	$block_menu = 0;
	$index = 0;
	$existing_tasks = isset($form['task']) ? $form['task'] : false;
	//block
	//use to re-order
	//loop existing data
	YB_Article_ReIndexMenuOrder::get_instance()->reIndex([
		'blocks' => $existing_tasks
	]);

	wp_die();
}

public function change_status()
{

	$status = 0;
	$submitted = 0;

	if( isset($_POST['status']) ){
		$status = $_POST['status'];
	}

	if($status != 0){
		$submitted = 1;
	}

	YB_Project_Pages::get_instance()->is_finished([
		'post_id' => $_POST['block_id'],
		'single' => true,
		'action' => 'u',
		'value' => $status,
	]);

	YB_Project_Pages::get_instance()->updateSubmitted($_POST['block_id'], $submitted);

	//status author complete
	if (  $status != 0 || $status > 0 ) {
		$postId = $_POST['block_id'];
		yb_reminder_clear(['block_id' => $postId]);
	}

	wp_die();
}

public function clear_reminder() {
	$return = [
		'status' => 0,
		'msg' => 'There was a problem in removing reminder.'
	];
	$postId = isset($_POST['post_id']) ? $_POST['post_id'] : false;
	if ( $postId && $postId != 0 ) {
		$authors = YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
			'post_id' => $postId,
			'action' => 'r',
			'single' => true
		]);
		if ( $authors ) {
			$res = yb_reminder_api_clear([
				'block_id' => $postId,
				'author_id' => $authors,
			]);
			if ( $res && $res['success'] == 1 ) {
				$return = [
					'status' => 1,
					'msg' => 'Deleted reminders notification.'
				];
			}
		}
	}
	echo json_encode($return);
	exit();
}

public function createNewArticle() {
	//parse the form inputs.
	parse_str($_POST['form'], $form);
	$yearbook_id = isset($form['block_yearbook_id']) ? $form['block_yearbook_id'] : false;
	if ( $yearbook_id ) {

		$post_status = $form['yearbook_status'];
		$block_title = $form['block_title'];

		//should be in utils
		//block size
		$block_size_fullpage = $form['block_size_fullpage'];
		$block_size_partpage = $form['block_size_partpage'];
		$custom_block_size_fullpage = $form['custom_block_size_fullpage'];

		$block_size = $block_size_fullpage . $block_size_partpage;
		if( $block_size_partpage == 0 ) {
			$block_size = $block_size_fullpage;
		}
		if ( isset($custom_block_size_fullpage) && ($block_size_fullpage == '-1' && $custom_block_size_fullpage != 0) ) {
			if ( $block_size_partpage == 0 ) {
				$block_size_partpage = '';
			}
			$block_size = (int)$custom_block_size_fullpage . $block_size_partpage;
		}
		//block size

		$menu_order = 0;
		if( ! isset($form['is_cover']) ) {
			$getLastPost = wp_get_recent_posts([
				'post_type' => YB_CPT_PREFIX,
				'post_parent' => $yearbook_id,
				'orderby' => 'menu_order',
				'numberposts' => 1
			], OBJECT);

			if ( $getLastPost ) {
				$menu_order = ( $getLastPost[0]->menu_order + 1 );
			}
		}

		$due_date = '';
		if ( isset( $form['due_date'] ) ) {
			$due_date = $form['due_date'];
		}

		$user_id = isset( $form['yearbook_school_admin_id'] ) ? $form['yearbook_school_admin_id'] : 0;
		$is_cover = isset( $form['is_cover'] ) ? $form['is_cover'] : 0;

		$assign_to = [];
		if ( isset( $form['post_author'] ) ) {
			$assign_to = $form['post_author'];
		}

		$insert_block_child_post = [
			'post_title' => $block_title,
			'post_type' => YB_CPT_PREFIX,
			'post_author' => $user_id,
			'post_parent' => $yearbook_id,
			'post_status' => $post_status,
			'menu_order' => $menu_order,
			'meta_input' => [
				'block_title' => $block_title,
				'due_date' => $due_date,
				'block_size_fullpage' => $block_size_fullpage,
				'block_size_partpage' => $block_size_partpage,
				'block_size' => $block_size,
				'submitted' => 0,
				'is_child_block' => 1,
				'is_finished' => 0,
				'school_admin_id' => $user_id,
				'yb_uniqid' => uniqid(),
				'is_cover' => $is_cover,
				'yb_multiple_authors' => $assign_to
			]
		];

		wp_insert_post($insert_block_child_post);

		wp_die();
	}
	wp_die();
}

public function __construct()
{
	add_action( 'wp_ajax_yb_create_yearbook', array($this, 'create_yearbook') );
	add_action( 'wp_ajax_yb_add_new_article', array($this, 'createNewArticle') );
	add_action( 'wp_ajax_yb_update_yearbook', array($this, 'update_yearbook') );
	add_action( 'wp_ajax_yb_update_section', array($this, 'update_section') );
	add_action( 'wp_ajax_yb_add_section', array($this, 'add_section') );
	add_action( 'wp_ajax_yb_sort_blocks', array($this, 'sort_blocks') );
	add_action( 'wp_ajax_yb_sort_sections', array($this, 'sort_sections') );
	add_action( 'wp_ajax_yb_change_status', array($this, 'change_status') );
	add_action( 'wp_ajax_yb_clear_reminder', array($this, 'clear_reminder') );
}

}
