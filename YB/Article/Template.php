<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 *
 * @since branch refactor-admin-view
 * */
class YB_Article_Template {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

  public function __construct()  {

  }

	public function articleStatus() {
		return [
			0 => 'On Going',
			1 => 'Author Complete',
			5 => 'Checked',
			2 => 'Proof Read',
			3 => 'Ready for Production',
			4 => 'In Production',
		];
	}

  public function pageOwner() {
    $account_model = new YB_Account_Model;
    return $account_model->get();
  }

  public function pageStatus() {
    return [
      'draft' => 'Draft',
      'publish' => 'Publish',
    ];
  }

  public function pageFullSize() {
    return [
      0 => 0,
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      '-1' => 'Custom',
    ];
  }

  public function pagePartSize() {
    return [
      0 => 0,
      '.25' => '1/4 Page',
      '.33' => '1/3 Page',
      '.50' => '1/2 Page',
      '.67' => '2/3 Page',
      '.75' => '3/4 Page',
    ];
  }

  public function pageContributors( $args = [] ) {
		$args = array(
			'meta_query' 	=> array(
					'relation' => 'OR',
					array(
						'key'     => 'yb_is_contributors',
						'value'   => 1,
						'compare' => '='
					),
					array(
						'key'     => 'yb_school_account',
						'value'   => 1,
						'compare' => '='
					)
			)
		 );

		$user_query = new WP_User_Query( $args );

		if ( ! empty( $user_query->get_results() ) ) {
			return $user_query->get_results();
		}
		return false;
  }

}
