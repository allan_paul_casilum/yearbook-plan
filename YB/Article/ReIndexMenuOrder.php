<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 *
 * @since branch refactor-admin-view
 * */
class YB_Article_ReIndexMenuOrder {
    /**
     * instance of this class
     *
     * @since 0.0.1
     * @access protected
     * @var	null
     * */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     0.0.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
            * - Uncomment following lines if the admin class should only be available for super admins
            */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()  {

    }

    public function reIndex($args = [])
    {
        $defaults = [
            'blocks' => []
        ];

        $args = wp_parse_args( $args, $defaults );
        $blocks = $args['blocks'];
        $block_menu = 0;
        $index = 0;
        //use to re-order
        //loop existing data
        if(isset($blocks['blocks'])) {
            foreach($blocks['blocks']['block_id'] as $k => $v) {
                $block_post_id = $v;
                if ( 
                    isset($blocks['blocks']['is_cover'][$index]) 
                    && ( $blocks['blocks']['is_cover'][$index] == 0 || $blocks['blocks']['is_cover'][$index] == '' )  
                ) {
                    $block_menu++;
                }
                $update_block_child_post = [
                    'ID' => $v,
                    'menu_order' => $block_menu,
                ];
                wp_update_post($update_block_child_post);
                $index++;
            }//loop $blocks['blocks']
        }//isset($blocks['blocks'])
        
    }

}
