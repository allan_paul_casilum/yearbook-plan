<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 *
 * @since branch refactor-admin-view
 * */
class YB_Article_Utils {

    /**
     * instance of this class
     *
     * @since 0.0.1
     * @access protected
     * @var	null
     * */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     0.0.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
            * - Uncomment following lines if the admin class should only be available for super admins
            */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()  {

    }

    //@param $articles should be instance of WP_Post
    public function toArticleArray($articles = [])
    {
        $res = [];
        if($articles){
            foreach($articles as $article){
                
                $postId = $article->ID;
    
                $is_cover = get_post_meta($postId, 'is_cover', true);
                $article_page_number = 0;
                $page_number_round = YB_Project_PagesMeta::get_instance()->yb_current_page([
                    'post_id' => $postId,
                    'action' => 'r',
                    'single' => true
                ]);
                
                if ( ! $is_cover ) {
                    $article_page_number = $page_number_round;
                }

                $get_block_size = YB_Project_PagesMeta::get_instance()->yb_block_size([
                    'action' => 'r',
                    'post_id' => $postId,
                    'single' => true
                ]);

                $res[$postId] = $article;
                $res[$postId]->page_number = $article_page_number;

            }
            wp_reset_postdata();
        }

        return $res;
    }
}
