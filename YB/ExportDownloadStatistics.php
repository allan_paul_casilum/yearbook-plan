<?php
if ( ! defined( 'WPINC' ) ) {
	die;
}

class YB_ExportDownloadStatistics {
    /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	public function __construct(){

	}

    public function getStatisticsAll($posts) {
        $postData = [];
        if( !empty($posts) ) {
            foreach($posts as $post) {
                $getMeta = YB_Project_PagesMeta::get_instance()->downloadExportStatistics([
                    'post_id' => $post->ID,
                    'prefix' => '_' . $post->ID,
                    'action' => 'r',
                ]);
                if($getMeta && !empty($getMeta)){
                    $postData[$post->ID] = $getMeta;
                }
            }
        }
        return $postData;
    }

    public function getStatisticsById($post_id) {
        $postData = [];
        if( !empty($post_id) ) {
            $getMeta = YB_Project_PagesMeta::get_instance()->downloadExportStatistics([
                'post_id' => $post_id,
                'prefix' => '_' . $post_id,
                'action' => 'r',
            ]);
            if($getMeta && !empty($getMeta)){
                $postData[$post_id] = $getMeta;
            }
            return $postData;
        }
        return false;
    }

    public function getLastDownloadDate($post_id)
    {
        $getLastDownloadDate = $this->getStatisticsById($post_id);
        if($getLastDownloadDate){
            return end($getLastDownloadDate[$post_id]);
        }
        return false;
    }
}