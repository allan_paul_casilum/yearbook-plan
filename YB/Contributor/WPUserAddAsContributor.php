<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Get the projects Task
 * @since 0.0.1
 * */
class YB_Contributor_WPUserAddAsContributor {
    /**
     * instance of this class
     *
     * @since 0.0.1
     * @access protected
     * @var	null
     * */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     0.0.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
            * - Uncomment following lines if the admin class should only be available for super admins
            */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {

    }

    public function init()
    {
        add_action('show_user_profile', [$this, 'allteams_user_profile_fields'] );
        add_action('edit_user_profile', [$this, 'allteams_user_profile_fields'] );
        add_action('user_new_form', [$this, 'allteams_user_profile_fields'] );
        add_action('personal_options_update', [$this, 'user_meta_save']);
        add_action('edit_user_profile_update', [$this, 'user_meta_save']);
        add_action('user_register', [$this, 'user_meta_save']);
    }

    public function allteams_user_profile_fields($user)
    {
        $getIsAContributor = get_user_meta($user->ID, 'user_add_as_contributor', true);
        $isAContributor = 0;
        if($getIsAContributor)
        {
            $isAContributor = $getIsAContributor;
        }
        
        ?>
        <h2>User Yearbook Options</h2>
        <table class="form-table">
            <tr>
                <th><label for="user_add_as_contributor">Add as Contributor?</label></th>
                <td>
                    <input
                        type="checkbox"
                        name="user_add_as_contributor"
                        id="user_add_as_contributor"
                        <?php echo ($isAContributor == 1) ? 'checked' : ''; ?>
                    >
                    <span class="description"></span>
                </td>
            </tr>
        </table>
        <?php
    }

    public function user_meta_save($userId)
    {
        $isContributor = 0;
        if($_POST && isset($_POST['user_add_as_contributor']) && $_POST['user_add_as_contributor'] == 'on')
        {
            $isContributor = 1;
        }
        update_user_meta($userId, 'user_add_as_contributor', $isContributor);
        YB_Contributor_Meta::get_instance()->is_contributors([
            'user_id' => $userId,
            'action' 	=> 'u',
            'value' 	=> $isContributor
        ]);
    }

}
