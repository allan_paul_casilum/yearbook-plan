<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Reminder Update Tag.
 * @since 0.0.1
 * */
class YB_ReminderUpdateTag {
  /**
   * instance of this class
   *
   * @since 0.0.1
   * @access protected
   * @var	null
   * */
  protected static $instance = null;

  /**
   * Return an instance of this class.
   *
   * @since     0.0.1
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  public function __construct() {

  }

	public function update( $args = [] ) {
		//tag format article_title . '-' . article_id . '-' . user_id
		$assignToArray = isset($args['assign_to']) ? $args['assign_to'] : false;
		if ( $assignToArray && count($assignToArray) > 0 ) {
			$newTitle = $args['block_title'];
			$oldTitle = $args['meta_block_title'];
			$aticleID = $args['yearbook_id'];
			$dueDate = $args['due_date'];
			foreach( $assignToArray as $userId ) {
				$oldTag = YB_ReminderClear::get_instance()->setTag([
					'article_title' => $oldTitle,
					'article_id'		=> $aticleID,
					'user_id'				=> $userId,
				]);
				YB_Reminder::get_instance()->clear([
					'tags' => $oldTag
				]);
				//re-add it
				$newTag = YB_ReminderClear::get_instance()->setTag([
					'article_title' => $newTitle,
					'article_id'		=> $aticleID,
					'user_id'				=> $userId,
				]);
				$articleLoginLink = YB_Project_Notify::get_instance()->loginlessUrl($aticleID, $userId);
				$user_info = get_userdata($userId);
				$data_api = [
					'due_date' => $dueDate,
					'article_title' => $newTitle,
					'tag' => $newTag,
					'send_to_details' => [
						'email' => $user_info->user_email,
						'user_name' => $user_info->display_name,
						'url' => $articleLoginLink
					],
					'send_to' => new YB_ReminderAPI_Member($user_info->user_email, $user_info->display_name, $articleLoginLink)
				];
				$res = YB_Reminder::get_instance()->send($data_api);
			}
		}
	}

}
