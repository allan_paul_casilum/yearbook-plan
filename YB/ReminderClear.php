<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Reminder Clear.
 * @since 0.0.1
 * */
class YB_ReminderClear {
  /**
   * instance of this class
   *
   * @since 0.0.1
   * @access protected
   * @var	null
   * */
  protected static $instance = null;

  /**
   * Return an instance of this class.
   *
   * @since     0.0.1
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  public function __construct() {

  }

	public function setTag($args = []) {
		//tag format article_title . '-' . article_id . '-' . user_id
		$articleTitle = isset( $args['article_title'] ) ? $args['article_title'] : '';
		$articleId 		= isset( $args['article_id'] ) ? $args['article_id'] : false;
		$userId 			= isset( $args['user_id'] ) ? $args['user_id'] : false;
		if ( $articleId ) {
			return $articleTitle . '-' . $articleId . '-' . $userId;
		}
		return false;
	}

	public function clearByAuthors( $args = [] ) {
		$postId 	= isset( $args['block_id'] ) ? $args['block_id'] : false;
		if ( $postId ) {
			$authors = YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
				'post_id' => $postId,
				'action' => 'r',
				'single' => true
			]);
			if ( $authors ) {
				$res = $this->clear([
					'block_id' => $postId,
					'author_id' => $authors,
				]);
				return $res;
			}
		}
		return false;
	}

	public function clear( $args = [] ) {
		$postId 	= isset( $args['block_id'] ) ? $args['block_id'] : false;
		$authors 	= $args['author_id'];

		if ( $postId ){
			$post = get_post( $postId );
			//tag format article_title . '-' . article_id . '-' . user_id
			$reminderApiTags = [];
			if ( $authors ) {
				$articleTitle = $post->post_title;
				$articleId 		= $postId;
				if ( is_array($authors) && count($authors) > 0 ) {
					foreach ( $authors as $author ) {
						$retTags = $this->setTag([
							'article_title' => $articleTitle,
							'article_id' 		=> $articleId,
							'user_id' 			=> $author
						]);
						if ( $retTags ) {
							$result = YB_Reminder::get_instance()->clear([
								'tags' => $retTags
							]);
							yb_remove_notifications_by_user_article_id($author, $articleId);
						}
					}
				} else {
					$retTags = $this->setTag([
						'article_title' => $articleTitle,
						'article_id' 		=> $articleId,
						'user_id' 			=> $authors
					]);
					if ( $retTags ) {
						$result = YB_Reminder::get_instance()->clear([
							'tags' => $retTags
						]);
						yb_remove_notifications_by_user_article_id($authors, $articleId);
					}
				}
				return $result;
			}
		}
		return false;
	}

}
