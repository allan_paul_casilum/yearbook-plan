<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Get the projects Task
 * @since 0.0.1
 * */
class YB_Project_Rest_Task {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;
	public $last_page = null;
	public $loop_page = 0;
	public $page_number = 1;
	public $total_block_size;
	public $loop_block_size_page;
	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function get_grandchildrens($post_id, $parent_data = [])
	{
		$args = array(
			'sort_order' => 'asc',
			'sort_column' => 'menu_order',
			'hierarchical' => 1,
			'child_of' => $post_id,
			'post_type' => YB_CPT_PREFIX,
			'post_status' => 'publish'
		);
		$pages = get_pages($args);
		if($pages) {
			$posts_data = [];
			foreach($pages as $k => $v) {
					$posts_data[] = [
						'id' => $v->ID,
						'post_author' => $v->post_author,
						'post_date' => $v->post_date,
						'post_title' => $v->post_title,
						'post_content' => $v->post_content,
						'post_status' => $v->post_status,
						'post_modified' => $v->post_modified,
						'post_parent' => $v->post_parent,
						'menu_order' => $v->menu_order,
						'comment_count' => $v->comment_count,
						'user_info' =>  get_userdata($v->post_author),
						'meta' => get_post_meta($v->ID),
						'parent_meta' => get_post_meta($v->post_parent)
				 ];
		 }
		}
		return $posts_data;
	}

	private function _get_posts_children($parent_id){
	    $children = [];
	    // grab the posts children
			$posts = get_posts( array(
				 'numberposts' => -1,
				 'post_status' => 'any',
				 'post_type' => YB_CPT_PREFIX,
				 'post_parent' => $parent_id,
				 'suppress_filters' => false,
				 'order' => 'ASC',
				 'orderby' => 'menu_order'
			 	)
			);
			$page_num = 2;
			if($posts) {
		    // now grab the grand children
				$obj_meta = new YB_Project_PagesMeta;
		    foreach( $posts as $child ){
					$due_date_standing = strtolower(allt_due_date_standing($obj_meta->getDueDate($child->ID)));
					$media = get_attached_media( '', $child->ID );
					$is_finished = YB_Project_Pages::get_instance()->is_finished([
						'post_id' => $child->ID,
						'single' => true
					]);
					$label_task = 'secondary';
					if($is_finished == 1){
						$label_task = 'success';
					}elseif($due_date_standing == 'overdue') {
						$label_task = 'warning';
					}elseif($due_date_standing == 'today') {
						$label_task = 'primary';
					}elseif($due_date_standing == 'upcoming') {
						$label_task = 'info';
					}
					$string_for_encrypt = YB_LoginLess::get_instance()->createCryptSalt(['task_id' => $child->ID, 'user_id' => $child->post_author]);
					$login_encrypt = YB_LoginLess::get_instance()->loginCrypt($string_for_encrypt);
					$children[] = [
					 'id' => $child->ID,
					 'post_author' => $child->post_author,
					 'post_date' => $child->post_date,
					 'post_title' => $child->post_title,
					 'post_status' => $child->post_status,
					 'post_modified' => $child->post_modified,
					 'post_parent' => $child->post_parent,
					 'menu_order' => $child->menu_order,
					 'comment_count' => $child->comment_count,
					 'page_number' => '1/'.$page_num,
					 'word_count' => str_word_count(wp_strip_all_tags($child->post_content)),
					 'photo_count' => count($media),
					 'due_date_standing' => $due_date_standing,
					 'due_date_format' => date("F j, Y", strtotime($obj_meta->getDueDate($child->ID))),
					 'due_date_standing_label' => $label_task,
 					 'due_date_human' => time2str($obj_meta->getDueDate($child->ID)),
 					 'is_finished' => $is_finished,
					 'user_info' =>  get_userdata($child->post_author),
					 'meta' => get_post_meta($child->ID),
					 'edit_post' => admin_url('post.php?post='.$child->ID.'&action=edit'),
					 'login_less_url' => site_url('/?show-task='.$login_encrypt),
				 ];
				 $page_num++;
		    }
	    }

	    return $children;
	}

	public function get($data) {
		$post_id = $data['id'];
		$post_data_meta = [];
		$posts = get_posts( array(
			 'numberposts' => -1,
			 'post_status' => 'any',
			 'post_type' => YB_CPT_PREFIX,
			 'post_parent' => $post_id,
			 'suppress_filters' => false,
			 'order' => 'ASC',
			 'orderby' => 'menu_order'
		 	)
		 );

		 if($posts) {
			 foreach($posts as $k => $v) {
				 $get_children = $this->_get_posts_children($v->ID);
				 $post_data_meta[] = [
					 'id' => $v->ID,
					 'post_author' => $v->post_author,
					 'post_date' => $v->post_date,
					 'post_title' => $v->post_title,
					 'post_status' => $v->post_status,
					 'post_modified' => $v->post_modified,
					 'post_parent' => $v->post_parent,
					 'menu_order' => $v->menu_order,
					 'comment_count' => $v->comment_count,
					 'count_children' => count($get_children),
					 'num_of_pages' => count($get_children),
					 'user_info' =>  get_userdata($v->post_author),
					 'meta' => get_post_meta($v->ID),
					 'children' => $get_children,
					 'edit_post' => admin_url('post.php?post='.$v->ID.'&action=edit'),
					 'is_single' => 0
				 ];
		 	}
		}
		 return $post_data_meta;
	 }

	 public function getSingle($data)
	 {
		 $post_id = $data['id'];
		 $post_data_meta = [];
		 //single
		 $posts = get_posts( array(
				'post_status' => 'any',
				'post_type' => YB_CPT_PREFIX,
				'post__in' => [$post_id],
				'suppress_filters' => false,
			 )
		 );
		 if($posts){
			 foreach($posts as $k => $v) {
					$post_data_meta[] = [
						'id' => $v->ID,
						'post_author' => $v->post_author,
						'post_date' => $v->post_date,
						'post_title' => $v->post_title,
						'post_status' => $v->post_status,
						'post_modified' => $v->post_modified,
						'post_parent' => $v->post_parent,
						'menu_order' => $v->menu_order,
						'comment_count' => $v->comment_count,
						'user_info' =>  get_userdata($v->post_author),
						'meta' => get_post_meta($v->ID),
						'edit_post' => admin_url('post.php?post='.$v->ID.'&action=edit'),
						'is_single' => 1
					];
			 }
		 }
		 return $post_data_meta;
	 }

	 public function showParentPage($data)
	 {
		 $post_id = $data['id'];
		 $posts = get_posts( array(
 			 'post_status' => 'any',
 			 'post_type' => YB_CPT_PREFIX,
 			 'post__in' => [$post_id],
 		 	)
 		 );
		 if($posts) {
			 $posts_data = [];
			 foreach($posts as $k => $v) {
				 $get_children = $this->_get_posts_children($v->ID);
				 $posts_data[] = [
					 'id' => $v->ID,
					 'post_author' => $v->post_author,
					 'post_date' => $v->post_date,
					 'post_title' => $v->post_title,
					 'post_status' => $v->post_status,
					 'post_modified' => $v->post_modified,
					 'post_parent' => $v->post_parent,
					 'menu_order' => $v->menu_order,
					 'comment_count' => $v->comment_count,
					 'count_children' => count($get_children),
					 'num_of_pages' => count($get_children),
					 'user_info' =>  get_userdata($v->post_author),
					 'meta' => get_post_meta($v->ID),
			 	];
		 	}
		 }
		 return $posts_data;
	 }

	public function getPages()
	{
		$args = array(
			'sort_order' => 'asc',
			'sort_column' => 'menu_order',
			'hierarchical' => 1,
			'child_of' => 0,
			'parent' => -1,
			'post_type' => YB_CPT_PREFIX,
			'post_status' => 'publish'
		);
		$pages = get_pages($args);
		if($pages) {
			$posts_data = [];
			foreach($pages as $k => $v) {
				if($v->post_parent == 0){
					$parent_data = [
						'section' => get_post_meta($v->ID, 'section', 1),
					];
					$posts_data[] = [
						'id' => $v->ID,
						'post_author' => $v->post_author,
						'post_date' => $v->post_date,
						'post_title' => $v->post_title,
						'post_content' => $v->post_content,
						'post_status' => $v->post_status,
						'post_modified' => $v->post_modified,
						'post_parent' => $v->post_parent,
						'menu_order' => $v->menu_order,
						'comment_count' => $v->comment_count,
						'user_info' =>  get_userdata($v->post_author),
						'meta' => get_post_meta($v->ID),
						'childrens' => $this->get_grandchildrens($v->ID, $parent_data)
				 ];
				}
		 }
		}
		return $posts_data;
	}

	public function getYearbookParent()
	{
		$post_data = [];
		$args = array(
			'posts_per_page'   => -1,
			'post_parent' => 0,
			'post_type' => YB_CPT_PREFIX,
			'post_status' => 'any'
		);

		if(!current_user_can('manage_options')) {
			$user_id = get_current_user_id();
			$args['author'] = $user_id;
			$args['meta_query'] = [
				'key' => 'school_admin_id',
				'value' => $user_id,
			];
		}
		$pages = new WP_Query( $args );
		if ( $pages->have_posts() ) {
			$post_data = $pages->posts;
			wp_reset_postdata();
		}

		return $post_data;
	}

	public function getSchoolAdminContributor($data)
	{
		$user_id = $data['id'];
		$data = [];
		$obj_contributors = new YB_Contributor_Model;
		$contributors = $obj_contributors->getById($user_id);
		foreach($contributors as $k => $v){
			$data[] = [
				'id' => $v->ID,
				'name' => $v->display_name,
			];
		}
		return $data;
	}

	public function get_v2($data)
	{
		$post_id = $data['id'];
		$post_data = [];
		$arg_parent = [
			'include' => $post_id,
			'post_type' => YB_CPT_PREFIX,
			'post_status' => 'any',
		];
		$parent_post   = get_posts( $arg_parent );
		if($parent_post) {
			$get_parent_post = $parent_post[0];
			$school_id = $get_parent_post->post_author;
			$this->loop_block_size_page = 0;
			$post_data = [
				'parent' => [
					'title' => $get_parent_post->post_title,
					'id' => $get_parent_post->ID,
					'user_id' => $school_id,
				],
				'blocks' => $this->_blocks($post_id, $school_id),
			];
		}
		return $post_data;
	}

	private function _blocks($parent_id, $school_id){
			$obj_meta = new YB_Project_PagesMeta;

	    $children = [];
	    // grab the posts children
			$posts = get_posts( array(
				 'numberposts' => -1,
				 'post_status' => 'any',
				 'post_type' => YB_CPT_PREFIX,
				 'post_parent' => $parent_id,
				 'suppress_filters' => false,
				 'order' => 'ASC',
				 'orderby' => 'menu_order',
			 	)
			);
			//print_r($posts);
			$loop_page_num = 0;

			$page_number = 1;
			if(!is_null($this->last_page)){
				$page_number = ($this->last_page += 1);
			}
			$previous_block_size = 0;
			if($posts) {
		    // now grab the grand children
				$i = 0;
		    foreach( $posts as $k => $child ){
					$left_side_css = '';
					$right_side_css = '';
					//compute block size
					$get_block_size = $obj_meta->yb_block_size([
						'action' => 'r',
						'post_id' => $child->ID,
						'single' => true
					]);
					$this->loop_block_size_page += $get_block_size;
					$loop_page_num += $get_block_size;
					//compute block size

					$due_date_standing = strtolower(allt_due_date_standing($obj_meta->getDueDate($child->ID)));
					$media = get_attached_media( '', $child->ID );
					$is_finished = YB_Project_Pages::get_instance()->is_finished([
						'post_id' => $child->ID,
						'single' => true
					]);
					$label_task = 'secondary';
					if($is_finished == 1){
						$label_task = 'success';
					}elseif($due_date_standing == 'overdue') {
						$label_task = 'warning';
					}elseif($due_date_standing == 'today') {
						$label_task = 'primary';
					}elseif($due_date_standing == 'upcoming') {
						$label_task = 'info';
					}
					$string_for_encrypt = YB_LoginLess::get_instance()->createCryptSalt(['task_id' => $child->ID, 'user_id' => $child->post_author]);
					$login_encrypt = YB_LoginLess::get_instance()->loginCrypt($string_for_encrypt);

					$compute_page_number = ($page_number + $loop_page_num);
					//echo $compute_page_number;
					if($page_number == 1) {
						$page_number_round = $loop_page_num;
					}else{
						$page_number_round = ($page_number + $get_block_size);
						//$page_number_round = number_format( ($compute_page_number - 1), 2);
					}
					$children['page'][$page_number]['compute_page_number'] = $page_number_round;
					$children['page'][$page_number]['all_block_size_page'] = $this->loop_block_size_page;
					$children['page'][$page_number]['current_block_size'] = $loop_page_num;
					$children['page'][$page_number]['page_block_size'] = fmod($loop_page_num, $page_number);
					//$children['page'][$page_number]['previous_block_size'] = ($previous_block_size + $get_block_size);

					$is_submitted  = YB_Project_Pages::get_instance()->isSubmitted($child->ID);
					$status = 'On Going';
					if($is_submitted){
						$status = 'Author Complete';
					}
					if($is_finished == 1){
						$status = 'Ready for Production';
					}
					if($is_finished == 2){
						$status = 'Proof Read';
					}

					$children['page'][$page_number][] = [
					 'id' => $child->ID,
					 'post_author' => $child->post_author,
					 'post_date' => $child->post_date,
					 'post_title' => $child->post_title,
					 'post_status' => $child->post_status,
					 'post_modified' => $child->post_modified,
					 'post_parent' => $child->post_parent,
					 'menu_order' => $child->menu_order,
					 'comment_count' => $child->comment_count,
					 'loop_page' => $this->loop_page,
					 'loop_page_num' => $loop_page_num,
					 'page_number' => $page_number_round,
					 'word_count' => str_word_count(wp_strip_all_tags($child->post_content)),
					 'photo_count' => count($media),
					 'due_date_standing' => $due_date_standing,
					 'due_date_format' => date("F j, Y", strtotime($obj_meta->getDueDate($child->ID))),
					 'due_date_standing_label' => $label_task,
 					 'due_date_human' => time2str($obj_meta->getDueDate($child->ID)),
 					 'is_finished' => $is_finished,
					 'status' => $status,
					 'user_info' =>  get_userdata($child->post_author),
					 'meta' => get_post_meta($child->ID),
					 'block_size_visual' => (100 * $get_block_size),
					 'previous_block_size' => (100 * $previous_block_size),
					 'loop_modulo' => ($page_number % 2),
					 'block_index' => $children['page'][$k],
					 'edit_post' => admin_url('post.php?post='.$child->ID.'&action=edit'),
					 'delete_url' => html_entity_decode(wp_nonce_url(yb_admin_url_yearbookpage("&_method=verify-delete-block&yb=".$parent_id."&school_id=".$school_id."&block_id=".$child->ID.""), "delete-block-".$child->ID.$school_id.$parent_id, "delete-block-nonce")),
					 'login_less_url' => site_url('/?show-task='.$login_encrypt),
				 ];

				 $obj_meta->yb_current_page([
					 'post_id' => $child->ID,
					 'action' => 'u',
					 'value' => $page_number_round
				 ]);

				 $obj_meta->yb_loop_block_size_page([
					 'post_id' => $child->ID,
					 'action' => 'u',
					 'value' => $loop_page_num
				 ]);

				 $this->last_page = $page_number;
				 if($loop_page_num >= 1) {
					 $loop_page_num = 0;
					 $page_number += 1;
				 }

				 $previous_block_size = $get_block_size;
				 $i++;
		    }
	    }

	    return $children;
	}

	public function sectionAutoComplete($data)
	{
		//print_r($data);
		$terms_data = [];
		//print_r($_GET);

		$term = isset($_GET['term']) ? $_GET['term'] : '';

		$school_id = $data['school_id'];
		$terms = get_terms(array(
		    'taxonomy' => 'section',
		    'hide_empty' => false,
		    'meta_query' => array(
		        [
		            'key' => 'yb_school_id_term',
		            'value' => $school_id
		        ]
		    )
		));

		if($terms) {
			foreach($terms as $k => $v) {
				$terms_data[$v->name] = [
					'value' => $v->name,
					'label' => $v->name,
					'slug' => $v->slug,
					'term_taxonomy_id' => $v->term_taxonomy_id,
					'term_id' => $v->term_id,
				];
			}
		}
		if(trim($term) != ''){
			$_terms_data = [];
			$search = $term.'*';
			$term_search = yb_array_key_exists_wildcard( $terms_data, $search );
			foreach($term_search as $k => $v) {
				$_terms_data[] = $terms_data[$v];
			}
			return $_terms_data;
		}else{
			return $terms_data;
		}
	}

  public function __construct()
  {
		add_action( 'rest_api_init', function () {
		  register_rest_route( 'yearbook/v1', '/get-yearbookplan/(?P<id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'get'),
		    'args' => array(
		      'id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-sections/(?P<school_id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'sectionAutoComplete'),
		    'args' => array(
		      'school_id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-yearbookplan-v2/(?P<id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'get_v2'),
		    'args' => array(
		      'id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-yearbookplan-single/(?P<id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'getSingle'),
		    'args' => array(
		      'id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		  register_rest_route( 'yearbook/v1', '/show-parent-page/(?P<id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'showParentPage'),
		    'args' => array(
		      'id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-pages', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'getPages'),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-yearbookplan-parent', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'getYearbookParent'),
		  ) );
		  register_rest_route( 'yearbook/v1', '/get-school-admin-contributor/(?P<id>\d+)', array(
		    'methods' => 'GET',
		    'callback' => array($this, 'getSchoolAdminContributor'),
				'args' => array(
		      'id' => array(
		        'validate_callback' => function($param, $request, $key) {
		          return is_numeric( $param );
		        }
		      ),
		    ),
		  ) );
		} );
  }

}
