<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Use for notify the user for the task.
 * @since 0.0.1
 * */
class YB_Project_Notify {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function _body($body)
	{

	}

	/**
	* Set the subject of the notification.
	**/
	public function _subject()
	{
		$str = 'Yolo - YearBook Task';
		return $str;
	}

	/**
	* The header of the email.
	**/
	public function _headers($args = [])
	{
		/**
		 * Define the array of defaults
		 */
		$defaults = [
			"From: Yolo Planit <message@planit.yolo.co.nz>",
			"Content-Type: text/html; charset=UTF-8"
		];

		/**
		 * Parse incoming $args into an array and merge it with $defaults
		 */
		$headers = wp_parse_args( $args, $defaults );
		return $headers;
	}

	/**
	* Sent notification viva ajax.
	**/
	public function ajax_notify()
	{

		if(isset($_POST['post_id'])) {
			$post_id = $_POST['post_id'];
			$parent_post_id = $_POST['yearbook_id'];
			$notify = $this->_notifyGroup($post_id, $_POST);
		}
		wp_die();
	}

	public function loginlessUrl($post_id, $author_id)
	{
		$string_for_encrypt = YB_LoginLess::get_instance()->createCryptSalt(['task_id' => $post_id, 'user_id' => $author_id]);
		$login_encrypt = YB_LoginLess::get_instance()->loginCrypt($string_for_encrypt);
		return site_url('/?show-task='.$login_encrypt);
	}

	//refactor

	public function _get_posts( $args = [] ) {
		$defaults = array (
			'include' 		=> [],
			'post_type' 	=> YB_CPT_PREFIX,
			'meta_query' 	=> [
				[
					'key' 		=> 'submitted',
					'value' 	=> 1,
					'compare' => '!='
				],
			]
    );

    // Parse incoming $args into an array and merge it with $defaults
    $args = wp_parse_args( $args, $defaults );
		$arr_post_id = [];
		$get_post = get_posts( $args );
		if ( $get_post ) {
			return $get_post;
		}
		return false;
	}

	public function _create_article( $args = [] ) {
		$defaults = array (
			'articles' => []
    );

    // Parse incoming $args into an array and merge it with $defaults
    $args = wp_parse_args( $args, $defaults );

		$articles = $args['articles'];
		$return_articles = [];

		if ( $articles && count( $articles ) > 0 ) {
			foreach( $articles as $k_post => $v_post ) {

				$authors = YB_Project_PagesMeta::get_instance()->yb_multiple_authors([
					'post_id' => $v_post->ID,
					'action' => 'r',
					'single' => true
				]);

				if ( $authors ) {

					foreach( $authors as $k_auth => $v_auth ) {
						$login_url = $this->loginlessUrl( $v_post->ID , $v_auth );
						$user_data = get_userdata( $v_auth );
						if ( $user_data ) {
							$user_email = $user_data->user_email;
							$user_name 	= $user_data->display_name;
							$user_id 		= $user_data->ID;

							$due_date = YB_Project_PagesMeta::get_instance()->due_date([
		            'action' 	=> 'r',
		            'post_id' => $v_post->ID,
		            'single' 	=> true
		          ]);

							$return_articles[$v_auth][] = [
								'user_id' 			=> $user_id,
								'to' 						=> $user_email,
								'user_name' 		=> $user_name,
								'article_id' 		=> $v_post->ID,
								'article_data' 	=> $v_post,
								'article_title' => $v_post->post_title,
								'due_date' 			=> $due_date,
								'login_url' 		=> $login_url,
								'subject' 			=> $this->_subject(),
								'headers' 			=> $this->_headers(),
							];

						}//if user_data
					}//foreach authors
				}//if authors

			}//171
		}//170
		return $return_articles;
	}//_create_article

	public function _create_data_to_send( $article_parent_id,  $args = [] ) {
		$data = [];
		// echo '_create_data_to_send'.'<br>';
		// apyc_dd($args);
		// echo '_create_data_to_send'.'<br>';
		foreach ( $args as $key => $value ) {
			$article_parent_id = $article_parent_id;

			$is_user_already_notified = YB_Project_PagesMeta::get_instance()->author_sent_notify([
				'post_id' => $article_parent_id,
				'user_id' => $value['user_id'],
				'action' 	=> 'r',
				'single' 	=> true
			]);

			$data['user_id'] 		= $value['user_id'];
			$data['article_id'] = $value['article_id'];
			$data['to'] 				= $value['to'];
			$data['user_name'] 	= $value['user_name'];
			$data['subject'] 		= $value['subject'];
			$data['is_notify'] 	= $is_user_already_notified ? 1:0;

			$data['articles'][] = [
				'article_id' 					=> $value['article_id'],
				'user_id' 						=> $value['user_id'],
				'article_title' 			=> $value['article_title'],
				'article_login_link' 	=> $value['login_url'],
				'due_date' 						=> $value['due_date'],
			];
			$data['headers'] 		= $value['headers'];

		}
		return $data;
	}//_create_data_to_send

	public function _set_body_mail( $data = [] ) {
		ob_start();
		YB_View::get_instance()->admin_partials('partials/mail/notify-group.php', $data);
		$body = ob_get_contents();
		ob_end_clean();

		return $body;
	}// _set_body_mail

	public function _author_sent_notify_article( $article_parent_id, $data = [] ) {
		if ( isset( $data ) && count( $data ) > 0 ) {
			$user_id = $data['user_id'];
		 	if ( is_array( $data['articles'] ) && count( $data['articles'] ) > 0 ) {
				foreach($data['articles'] as $article) {
					YB_Project_PagesMeta::get_instance()->author_sent_notify_article([
						'post_id' => $article_parent_id,
						'user_id' => $user_id . '_' . $article['article_id'],
						'action' 	=> 'u',
						'value' 	=> 1
					]);
				}//foreach $data
			}// if is_array $data
		}//if $data

	}// _author_sent_notify_article

	public function _get_not_sent_articles( $args = [] ) {
		$data 								= $args['data'];
		$article_parent_id 		= $args['article_parent_id'];
		$ret_data = [];
		//apyc_dd($data);
		if ( isset( $data ) && count( $data ) > 0 ) {
			$user_id = $data['user_id'];
			if ( is_array( $data['articles'] ) && count( $data['articles'] ) > 0 ) {
				foreach( $data['articles'] as $k => $article ) {
					$exists = YB_Project_PagesMeta::get_instance()->author_sent_notify_article([
						'post_id' => $article_parent_id,
						'user_id' => $user_id . '_' . $article['article_id'],
						'action' 	=> 'r',
						'single' 	=> true
					]);
					if ( ! $exists ) {
						$ret_data[$user_id]['to'] = $data['to'];
						$ret_data[$user_id]['user_name'] = $data['user_name'];
						$ret_data[$user_id]['subject'] = $data['subject'];
						$ret_data[$user_id]['article_id'] = $data['article_id'];
						$ret_data[$user_id]['user_id'] = $data['user_id'];
						$ret_data[$user_id]['headers'] = $data['headers'];
						$ret_data[$user_id]['is_notify'] = 0;
						$ret_data[$user_id]['articles'][] = $data['articles'][$k];
					}//if !exists
				}//foreach $data articles
			}//foreach $data

		}//if isset $data

		return $ret_data;
	}//_get_not_sent_articles

	public function _send_new_assign_notification( $args = [] ) {
		$defaults = array (
      'data' 							=> [],
			'article_parent_id' => false
    );

    // Parse incoming $args into an array and merge it with $defaults
    $args = wp_parse_args( $args, $defaults );
		$data = $args['data'];
		$notify_mode = isset($args['notify_mode']) ? $args['notify_mode'] : '';
		$article_parent_id = $args['article_parent_id'];
		if( $article_parent_id
				&& $data
				&& count( $data ) >= 1
		) {
			//apyc_dd($data, 1);
			foreach( $data as $key => $val) {
				if ( is_array( $val ) && count($val) > 0 ) {
					foreach( $val as $k => $v ) {
						//apyc_dd($v);
						$body = $this->_set_body_mail( $v );

						$is_user_already_notified = $v['is_notify'];
						$post_id = $v['article_id'];
						$user_id = $v['user_id'];

						$sent = false;
						$sent_to_authors = [];
						$sent_to_user = false;
						if ( $notify_mode == 'manual' ) {
							$sent_to_user = true;
						} elseif ( !$is_user_already_notified || $is_user_already_notified == 0 ) {
							$sent_to_user = true;
						}

						if ( $sent_to_user ) {

							do_action( 'yb_send_notification_before', $v );

							$sent = wp_mail(
								$v['to'],
								$v['subject'],
								$body,
								$v['headers']
							);

							//add reminder here
							do_action( 'yb_send_notification_after', $v );

							if ( $sent ) {
								YB_Project_PagesMeta::get_instance()->author_sent_notify([
									'post_id' => $article_parent_id,
									'user_id' => $v['user_id'],
									'action' 	=> 'u',
									'value' 	=> 1
								]);

								if ( isset( $v ) && count( $v ) > 0 ) {
									$this->_author_sent_notify_article( $article_parent_id, $v );
								}// if isset $val

							}// if $sent
						}// if $sent_to_user

					}//foreach $val
				}// if is array $val
			}//foreach $data

		}// if exists and true

	}//_send_new_assign_notification

	public function _notifyGroup($parent_post_id, $input_post = []) {

		$data 				= [];
		$articles 		= [];
		$not_sent_arr = [];

		$posts_id = $parent_post_id;

		$article_parent_id = 0;
		if ( isset( $input_post['yearbook_id'] ) ) {
			$article_parent_id = $input_post['yearbook_id'];
		}

		$notify_mode = false;
		if ( isset( $input_post['notify_mode'] ) ) {
			$notify_mode = $input_post['notify_mode'];
		}

		if ( ! is_array( $parent_post_id ) ) {
			$posts_id = [$parent_post_id];
		}

		$get_post_args = [
			'include' => $posts_id
		];
		$ret_posts_articles = $this->_get_posts( $get_post_args );
		// apyc_dd($get_post_args);
		// apyc_dd($ret_posts_articles, 1);
		if ( $ret_posts_articles ) {
			//create articles array
			$res_articles = $this->_create_article([
				'articles' => $ret_posts_articles
			]);

			if ( $res_articles && count( $res_articles ) >= 1 ) {
				foreach ( $res_articles as $k_send => $v_send ) {
					$res_articles_send_arr = [];
					//prepare articles data to send, output is array
					if ( is_array( $v_send ) ) {
						$res_articles_send_arr = $this->_create_data_to_send( $article_parent_id, $v_send );
					}// if is_array $v_send

					if( $res_articles_send_arr && count( $res_articles_send_arr ) >= 1 ) {

						$body = $this->_set_body_mail( $res_articles_send_arr );

						$is_user_already_notified = $res_articles_send_arr['is_notify'];
						$post_id 									= $res_articles_send_arr['article_id'];
						$user_id 									= $res_articles_send_arr['user_id'];

						$sent 						= false;
						$sent_to_authors 	= [];
						$sent_to_user 		= false;

						if ( $notify_mode == 'manual' ) {
							$sent_to_user = true;
						} elseif ( ! $is_user_already_notified || $is_user_already_notified == 0 ) {
							$sent_to_user = true;
						}

						if ( $sent_to_user ) {
							do_action( 'yb_send_notification_before', $res_articles_send_arr );

							$sent = wp_mail(
								$res_articles_send_arr['to'],
								$res_articles_send_arr['subject'],
								$body,
								$res_articles_send_arr['headers']
							);

							//add reminder here
							do_action( 'yb_send_notification_after', $res_articles_send_arr );

							if ( $sent ) {
								YB_Project_PagesMeta::get_instance()->author_sent_notify([
									'post_id' => $article_parent_id,
									'user_id' => $res_articles_send_arr['user_id'],
									'action' 	=> 'u',
									'value' 	=> 1
								]);
								$this->_author_sent_notify_article( $article_parent_id, $res_articles_send_arr );
							} //if $sent
						} else {
							//get the new article that the current contributor is assigned.
							$not_sent_arr[] = $this->_get_not_sent_articles([
								'data' 							=> $res_articles_send_arr,
								'article_parent_id' => $article_parent_id
							]);
						} // if $sent_to_user
					}//if $res_articles_send_arr
					//apyc_dd($res_articles_send_arr);
				}//foreach $res_articles
			}//if $res_articles
		}//if $ret_posts_articles

		if (	$not_sent_arr && count( $not_sent_arr ) >= 1 ) {
			$this->_send_new_assign_notification([
				'data' 							=> $not_sent_arr,
				'article_parent_id' => $article_parent_id
			]);
		}
	}// _notifyGroup
	//refactor

	//will be removed


	/**
	* initialize ajax.
	**/
	public function init_ajax()
	{
		add_action( 'wp_ajax_task_notify', array($this, 'ajax_notify') );
	}

	public function __construct()
	{

	}

}
