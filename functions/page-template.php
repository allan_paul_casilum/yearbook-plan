<?php
//branch refactor-admin-view
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function yb_get_full_page_size() {
	return YB_Article_Template::get_instance()->pageFullSize();
}

function yb_get_part_page_size() {
	return YB_Article_Template::get_instance()->pagePartSize();
}

function yb_page_status() {
	return YB_Article_Template::get_instance()->pageStatus();
}

function yb_page_author_status() {
	return YB_Article_Template::get_instance()->articleStatus();
}

function yb_page_block( $args = [] ) {
	$fields = yb_verbage('fields');
	$data 	= $args;
	$data['verbage_fields'] = $fields;
	YB_View::get_instance()->admin_partials('partials/page/part/page-block.php', $data);
}

function yb_page_contributors( $args = [] ) {
	return YB_Article_Template::get_instance()->pageContributors();
}

function yb_default_new_block_page( $args = [] ) {
	$action 							= $args['action'];
	$parent_post_id 			= isset($args['parent_post_id']) ? $args['parent_post_id'] : 0;
	$post_id 							= isset($args['post_id']) ? $args['post_id'] : 0;
	$post_status					= isset($args['status']) ? $args['status'] : 'draft';
	$is_cover 						= isset($args['is_cover']) ? $args['is_cover'] : 1;
	$menu_order 					= isset($args['menu_order']) ? $args['menu_order'] : 0;
	$assign_to 						= isset($args['assign_to']) ? $args['assign_to'] : 0;
	$db_assign_to 				= isset($args['db_assign_to']) ? $args['db_assign_to'] : [];
	$due_date 						= isset($args['due_date']) ? $args['due_date'] : '';
	$is_finished 					= isset($args['is_finished']) ? $args['is_finished'] : 0;
	$block_size_fullpage 	= isset($args['block_size_fullpage']) ? $args['block_size_fullpage'] : 1;
	$block_size_partpage 	= isset($args['block_size_partpage']) ? $args['block_size_partpage'] : 0;
	$block_size 					= isset($args['block_size']) ? $args['block_size'] : 1;
	$school_admin 				= isset($args['school_admin']) ? $args['school_admin'] : 0;

	$default_blocks = [
		[
			'title' => 'Front Cover',
		],
		[
			'title' => 'Inside Front Cover',
		],
		[
			'title' => 'Inside Back Cover',
		],
		[
			'title' => 'Back Cover',
		],
	];
	$index = 0;
	foreach( $default_blocks as $block ) {
		$create_block = [
			'post_title' => $block['title'],
			'post_parent' => $parent_post_id,
			'post_status' => $post_status,
			'post_type' => YB_CPT_PREFIX,
			'meta_input' => [
				'school_admin_id' => $school_admin,
				'block_title' => $block['title'],
				'due_date' => '',
				'block_size_fullpage' => $block_size_fullpage,
				'block_size_partpage' => $block_size_partpage,
				'block_size' => $block_size,
				'submitted' => 0,
				'is_child_block' => 1,
				'is_finished' => 0,
				'school_admin_id' => $school_admin,
				'yb_uniqid' => uniqid(),
				'is_cover' => $is_cover
			]
		];
		wp_insert_post($create_block);
	}

}
