<?php
//branch refactor-admin-view
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function ybGetLoopPartTemplate( $args = [] ) {
	$defaults = array (
			'yearbook_id' => 0,
			'post_status' => '',
			'blocks' => [],
			'post_id' => 0,
			'pageNumber' => '',
			'isCover' => 0,
			'can_be_edited' => false,
			'deleteUrl' => '',
			'editUrl' => '',
			'menu_order' => 0,
			'block_size' => 0,
			'post_title' => '',
			'authors_name' => '',
			'word_count' => 0,
			'photo_count' => 0,
			'is_finished' => 0,
			'due_date_standing' => '',
			'due_date_format' => '',
			'block_status' => '',
			'page_status' => [],
			'contributors' => [],
	);

	// Parse incoming $args into an array and merge it with $defaults
	$args = wp_parse_args( $args, $defaults );

	if (
		$args['yearbook_id'] != 0
		&& $args['blocks']
		&& count($args['blocks']) > 0
	) {
		YB_View::get_instance()->admin_partials('partials/article/part/list-loop.php', $args);
	}
}
