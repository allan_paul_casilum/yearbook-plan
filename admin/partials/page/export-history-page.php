<div class="bootstrap-iso">
    <div class="wrap" id="yb-export-history-page">
        <h3 class="yb-title"><?php echo $page_title; ?></h3>
        
        <h5 class="yb-sub-title"><?php echo $goback; ?></h5>
        <div class="yp-export-page-history-container">
            <div class="yb-history-download-data">
                <?php if($history) : ?>
                    <h5 class="yb-sub-title">Download History</h5>
                    <ul class="yb-list-data">
                        <?php foreach($history[$post_id] as $post ) : ?>
                            <li class="yb-list-data-items">
                                    <span style="font-size:15px;"><?php echo $post['date'];?> by <?php echo $post['who'];?> ( <?php echo $post['filename_zip'];?> )</span>
                                </li>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>