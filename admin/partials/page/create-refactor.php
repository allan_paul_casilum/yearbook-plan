<div class="bootstrap-iso">
  <div class="wrap">
    <div id="loader"></div>
    <h3><?php echo $plugin_page_title;?></h3>
    <div class="container-fluid">
      <div class="allteams-sendtask form-group">
      <div class="container-fluidx">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <form name="create-new-yearbook-plan" id="yearbook-plan-form" action="<?php echo $action_url;?>" method="post">
              <input type="hidden" name="_method" value="<?php echo $method;?>">
              <input type="hidden" name="yearbook_id" value="<?php echo $yearbook_id;?>" id="yearbook_id">
              <div class="row">
                <div class="col-sm-12 col-md-12 yb-right-container"> <!-- right -->
                  <div class="form-row">
                      <div class="col">
                        <label for="yearbook-page-name"><?php echo $verbage_fields['yearbook_name'];?></label>
                        <input type="text" class="form-control form-control-sm yearbook-page-name" value="<?php echo $name;?>" name="yearbook-page-name" id="yearbook-page-name" >
                      </div>
                      <div class="col">
                        <?php if($is_admin && $status == 'draft') { ?>
                          <div class="form-group">
                            <label for="selectAccount">Owner</label>
                            <?php if($accounts) { ?>
                            <select name="school_admin" id="school_admin" class="form-control input-sm">
                            <?php foreach($accounts as $k => $v) { ?>
                            <option value="<?php echo $v->ID;?>" <?php echo ($school_admin_id == $v->ID) ? 'selected':''; ?>><?php echo $obj_account_meta->account_name(['user_id'=>$v->ID,'single'=>true]);?></option>
                            <?php } ?>
                            </select>
                            <?php } ?>
                          </div>
                        <?php }else{ ?>
                          <?php if($is_admin && $status == 'publish') { ?>
                            <div class="form-group">
                              <label for="selectAccount"><?php echo $verbage_fields['school_owner'];?></label>
                              <input type="text" name="school_name" class="form-control input-sm" id="school_name" value="<?php echo isset($school_name->display_name) ? $school_name->display_name : '';?>" readonly>
                            </div>
                          <?php } ?>
                          <input type="hidden" name="school_admin" id="school_admin" value="<?php echo $school_admin_id;?>">
                        <?php } ?>
                      </div>

                      <div class="col">
                        <div class="form-group">
                          <div class="form-group">
                            <label for="status"><?php echo $verbage_fields['yearbook_status'];?></label>
                            <select name="status" class="form-control form-control-sm status" id="status">
                            <option value="draft" <?php echo ($status == 'draft') ? 'selected':''; ?>><?php echo $verbage_fields['yearbook_status_draft'];?></option>
                            <option value="publish" <?php echo ($status == 'publish') ? 'selected':''; ?>><?php echo $verbage_fields['yearbook_status_publish'];?></option>
                            </select>
                          </div>
                          <input type="submit" name="publish" id="publish" class="btn btn-primary btn-sm" value="<?php echo $verbage_fields['publish_page'];?>">
                          <?php
                            if ( is_super_admin() ) {
                              yb_show_export_button($yearbook_id, $school_admin_id, $status);
                              //yb_show_preview_button($yearbook_id, $school_admin_id, $status);
                            }
                          ?>
                      </div>
                    </div>
                  </div>
                </div><!-- right -->

                <div class="col-sm-12 col-md-12 yb-left-container"><!-- left -->
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="bulk-edit-container"></div>
                      <button class="btn btn-primary btn-primary bulk-edit-btn btn-sm" name="bulk-edit" >Bulk Edit</button>
                      <?php if ( is_super_admin() ) : ?>
                        <?php $bare_url = admin_url('?page=YearBook&_method=cleandb&id='.$yearbook_id.'&school_id='.$school_admin_id); ?>
                        <?php $nonce_clean_db = wp_nonce_url( $bare_url, 'cleandb-page_id-' . $yearbook_id . '-school-id-' . $school_admin_id, 'cleandb-nonce' ); ?>
                        <a href="<?php echo $nonce_clean_db; ?>" class="btn btn-danger cleanup-db btn-sm" name="clean-db" >Clean DB</a>
                      <?php endif; ?>
                    </div>
                  </div>

                  <table class="wp-list-table table widefat fixed striped pages">
                    <thead>
                      <tr>
                        <th scope="col" style="width:15%;"><input type="checkbox" id="checkAll"/></th>
                        <th scope="col">Article Position</th>
                        <th scope="col"><?php echo $verbage_fields['column_page'];?></th>
                        <th scope="col"><?php echo $verbage_fields['column_article_name'];?></th>
                        <th scope="col"><?php echo $verbage_fields['column_author'];?></th>
                        <th scope="col"><?php echo $verbage_fields['column_word_count'];?></th>
                        <th scope="col"><?php echo $verbage_fields['column_photo_count'];?></th>
                        <th scope="col"><?php echo $verbage_fields['column_due_date'];?></th>
                        <!--<th scope="col">Page Side</th>-->
                        <th scope="col"><?php echo $verbage_fields['column_status'];?></th>
                      </tr>
                    </thead>
                    <tbody class="content-placeholder sortable">
                      <?php
                        ybGetLoopPartTemplate([
                          'yearbook_id' => $yearbook_id,
                          'blocks' => $blocks,
                          'post_status' => $status,
                          'school_admin_id' => $school_admin_id
                        ]);
                      ?>
                    </tbody>
                  </table>

                  <div class="container yb-add-container">
                    <div class="block-action">
                    </div>
                  </div>

                  <p></p>
                  <?php if ( $method == 'update' ) : ?>
                    <button type="button" class="btn btn-success btn-sm add-block"><?php echo $verbage_fields['add_blocks'];?></button>
                  <?php elseif ( $method == 'insert' ) : ?>
                    <input type="submit" name="publish" id="publish" class="btn btn-primary btn-sm" value="<?php echo $verbage_fields['publish_page'];?>">
                  <?php endif; ?>
                  <p></p>
                </div><!-- left -->
              </div>
            </form>
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container -->
      </div><!-- allteams-sendtask -->
    </div><!-- container-fluid -->
  </div><!-- wrap -->
</div><!-- bootstrap-iso -->
