<div class="col-md-12 <?php echo $action;?>-div-col-container">
  <div class="edit-div-<?php echo $post_id;?> edit-div-container">
    <div class="row row-blocks update-block-container block-<?php echo $index;?>" data-block-id="<?php echo $post_id;?>">
        <div class="form-group col-md-3 size-order-container">
          <div class="row edit-div-row">
            <div class="col-md-12 edit-div-row-col">
              <div class="form-row edit-div-form-row">
                <div class="form-group col-md-6 size_fullpage-container">
                  <label for="block-size_fullpage" class="label-block-size_fullpage"><?php echo $verbage_fields['block_size_fullpage'];?></label>
                  <?php
                    $block_size_fullpages = yb_get_full_page_size();
                  ?>
                  <select class="form-control form-control-sm block-size_fullpage" name="update_task[block][block_size_fullpage]">
                    <?php foreach( $block_size_fullpages as $key => $val ) : ?>
                      <option value="<?php echo $key;?>" ><?php echo $val; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group col-md-6 size_partpage-container">
                  <label for="block-size_partpage" class="label-block-size_partpage"><?php echo $verbage_fields['block_size_partpage'];?></label>
                  <?php
                    $block_size_partpages = yb_get_part_page_size();
                  ?>
                  <select class="form-control form-control-sm block-size_partpage" name="update_task[block][block_size_partpage]">
                    <?php foreach( $block_size_partpages as $key => $val ) : ?>
                      <option value="<?php echo $key;?>" ><?php echo $val; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 custom-page-size-form-container">
              <div class="custom-page-size-container" style="<?php echo ( $block_size_fullpage !== '-1' ) ? 'display:none;' : '';?>">
                  <label for="custom-block-size" class="custom-block-size-label">Custom Size</label>
                  <input type="text" class="form-control form-control-sm custom-block-size" name="update_task[block][custom_block_size_fullpage]" autocomplete="off" value="<?php echo $block_size;?>">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-6 menu-order-form-container">
              <div class="menu-order-container">
                  <?php if ( $is_cover !== 1 ) : ?>
                    <label for="block-menu_order" class="block-menu_order-label">Article Position</label>
                    <input class="form-control form-control-sm block-menu_order" name="update_task[block][menu_order]" type="text" value="<?php echo $menu_order;?>">
                  <?php endif; ?>
              </div>
            </div>
            <div class="form-group col-md-6 is-cover-container">
              <?php if ( is_super_admin() ) : ?>
                <label for="block-is_cover" class="is-cover-label">Is Cover?</label>
                <input class="form-checkbox block-is_cover" name="update_task[block][is_cover]" type="checkbox" value="1" <?php echo ($is_cover ==1 ) ? 'checked':'';?> >
              <?php else: ?>
                  <input class="block-cover block-is_cover" name="update_task[block][is_cover]" type="hidden" value="<?php echo $is_cover;?>" >
              <?php endif; ?>
            </div>
          </div>

        </div>
        <div class="form-group col-md-3 article-name-container">
          <label for="NameInput" class="article-title-label"><?php echo $verbage_fields['block_title'];?></label>
          <div class="row article-title-container">
            <input type="text" class="form-control form-control-sm block-title" name="update_task[block][block_title]" autocomplete="off" value="<?php echo $title;?>">
          </div>
        </div>

        <div class="form-group col-md-3 assign_to-container">
          <div>
          <?php
            $contributors = yb_page_contributors();
          ?>
          <label for="NameInput" class="assign-to-label"><?php echo $verbage_fields['assign_to'];?></label></div>
          <select class="selectpicker assign_to" id="assign_to-<?php echo $post_id;?>" name="update_task[block][post_author][]" multiple data-selected-text-format="count > 2" data-live-search="true" data-actions-box="true">
            <?php foreach( $contributors as $key => $contributor ) : ?>
              <option value="<?php echo $contributor->ID;?>" ><?php echo $contributor->display_name;?></option>
            <?php endforeach; ?>
          </select>
          <input type="hidden" name="assign_to_input" value="" class="assign_to_input">
        </div>
        <div class="form-group col-md-2 due_date-container">
          <label for="NameInput" class="due_date-label"><?php echo $verbage_fields['due_date'];?></label>
          <input type="text" class="form-control form-control-sm due_date" name="update_task[block][due_date]" value="" autocomplete="off">
        </div>

      <?php if ( $action !== 'insert' ) : ?>
      <div class="form-group col-md-12">
        <div class="alert alert-primary update-block-ajax-msg" role="alert"></div>
        <div class="btn-action">
          <?php if ( $is_finished == 0 ) : ?>
            <a href="#" data-post-id="<?php echo $post_id;?>" class="btn-sm btn-primary task-notify ">Send Notification</a>
          <?php endif; ?>
          <a href="#" class="update-block btn-sm btn-primary new-block-btn" data-block-id="<?php echo $post_id;?>">Update</a>
          <a href="#" class="cancel-update-block btn-sm btn-primary new-block-btn">Cancel</a>
        </div>
      </div>
      <?php endif; ?>

    </div>
  </div>
</div>
