<div class="bootstrap-iso">
    <div class="wrap" id="yb-pick-articles-to-download">
        <h3 class="yb-title">Pick Articles to Download</h3>
        <h4 class="yb-sub-title"><?php echo $goback; ?></h4>

        <div class="yb-export-page">
            <form action="<?php echo admin_url('?page=YearBook&_method=processExportYearBookPage&site-id='.$site_id.'&article-id='.$article_id.'&school-id='.$schoold_id.'');?>" method="POST" class="yb-ready-for-production-form">
                <div class="yb-ready-for-production-container">
                    <h4 class="yb-title">Ready for Production</h4>
                    <p>(<?php echo $count_release;?>) Ready for Production</p>
                    <?php if($released_data) : ?>
                        <ul class="yb-list-data">
                            <?php foreach($released_data as $post ) : ?>
                                    <li class="yb-list-data-items">
                                        <input type="checkbox" name="release[]" value="<?php echo $post->ID;?>" checked>
                                        <span style="font-size:15px;">Page <?php echo $post->page_number;?> - <?php echo $post->post_title;?></span>
                                    </li>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </ul>
                    <?php endif; ?>
                </div>

                <div class="yb-in-production-container">
                    <h4 class="yb-title">In Production</h4>
                    <p>(<?php echo $count_in_production;?>) In Production Articles</p>
                    <?php if($in_production_data) : ?>
                        <ul class="yb-list-data">
                            <?php foreach($in_production_data as $post ) : ?>
                                    <?php $lastDownloadStr = ''; ?>
                                    <li class="yb-list-data-items">
                                        <?php 
                                            $getLastDownloadDate = YB_ExportDownloadStatistics::get_instance()->getLastDownloadDate($post->ID);
                                            if(isset($getLastDownloadDate['date'])){
                                                $urlShowHistory = wp_nonce_url( admin_url('?page=YearBook&_method=historyExportYearBookPage&site-id='.$site_id.'&article-id='.$article_id.'&school-id='.$schoold_id.'&post_id='.$post->ID.'&pagenum='.$post->page_number.'&pagetitle='.$post->post_title.''), 'show-history-' . $post->ID );
                                                //$lastDownloadStr = '(last download '.date('d/m/Y H:i A', strtotime($getLastDownloadDate['date'])).') '.' <a href="'.$urlShowHistory.'">See All</a>';
                                                $lastDownloadStr = '(last download '.$getLastDownloadDate['date'].') '.' <a href="'.$urlShowHistory.'">See All</a>';
                                            }
                                        ?>

                                        <input type="checkbox" name="in_production[]" value="<?php echo $post->ID;?>">
                                        <span style="font-size:15px;">Page <?php echo $post->page_number;?> - <?php echo $post->post_title;?> <?php echo $lastDownloadStr;?></span>
                                    </li>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <?php if( $count_release > 0 || $count_in_production > 0 ) : ?>
                    <input type="hidden" name="site_id" value="<?php echo $site_id;?>">
                    <input type="hidden" name="article_id" value="<?php echo $article_id;?>">
                    <input type="hidden" name="school_id" value="<?php echo $schoold_id;?>">
                    <input type="submit" value="Export"></input>
                <?php endif; ?>
            </form>      
        </div>

    </div>
</div>