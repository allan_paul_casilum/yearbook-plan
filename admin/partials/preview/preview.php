<div class="bootstrap-iso">
  <div class="wrap">
    <div class="preview-container">
        <div class="preview-body">
            <div class="preview-parent-article">
                <h1 class="display-1"><?php echo $blocks['parent']['title'];?></h1>
            </div>
            <?php //apyc_dd($blocks['blocks']); ?>
            <div class="preview-article-items">
                <?php if(isset($blocks['blocks']) && !empty($blocks['blocks'])) : ?>
                    <?php foreach($blocks['blocks'] as $block) : ?>
                        <div class="preview-block">
                            <p class="preview-block-title"><?php echo $block['post_title']; ?></p>
                            <div class="preview-block-content">
                                <?php echo $block['post_content']; ?>
                            </div>
                            <div class="preview-block-images">
                                <?php if(isset($block['images']) && !empty($block['images'])) : ?>
                                    <div class="card-deck">
                                        <?php foreach($block['images'] as $image) : ?>
                                            <div class="card"><img class="card-img-top" src="<?php echo $image[0];?>" alt="Images"></div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <hr class="mt-2 mb-3"/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
  </div>
</div>