<div class="bootstrap-iso">
  <div class="wrap">
    <div class="preview-container">
        <div class="preview-body">
            <div class="preview-article-items">
                <div class="preview-block">
                    <h1 class="preview-block-title"><?php echo $posts->post_title; ?></h1>
                    <div class="preview-block-content">
                        <?php echo $posts->post_content; ?>
                    </div>
                    <div class="preview-block-images">
                        <?php if(!empty($attachments)) : ?>
                            <div class="card-deck">
                                <?php foreach($attachments as $image) : ?>
                                    <div class="card">
                                        <?php if( wp_attachment_is_image($image->ID) ) : ?>
                                        <?php echo wp_get_attachment_image( $image->ID, "medium", "", array( "class" => "card-img-top" ) );  ?>
                                        <?php else : ?>
                                        <p class="card-text"><?php echo basename( get_attached_file($image->ID) );?></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>