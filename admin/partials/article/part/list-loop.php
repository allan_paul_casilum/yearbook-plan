<?php if ( $blocks && count($blocks['blocks']) > 0 ) : ?>
  <?php
    $contributors = yb_page_contributors();
    $verbage_fields = yb_verbage('fields');
    $page_status = yb_page_author_status();
    $templatePageFullSizes = yb_get_full_page_size();
    $templatePagePartSizes = yb_get_part_page_size();
  ?>
  <?php foreach( $blocks['blocks'] as $block ) : ?>
    <?php
      $post_id = $block['id'];
      $pageNumber = $block['page_number'];
      $isCover = 0;
      if ( isset($block['meta']['is_cover']) && $block['meta']['is_cover'][0] && $block['meta']['is_cover'][0] != 0 ) {
        $isCover = $block['meta']['is_cover'][0];
      }
      $can_be_edited = $block['can_be_edited'];
      $deleteUrl = $block['delete_url'];
      $editUrl = $block['edit_post'];
      $menu_order = $block['menu_order'];

      $block_size = isset( $block['meta']['block_size'][0] ) ? $block['meta']['block_size'][0] : 0;
      $block_part_size = isset( $block['meta']['block_size_partpage'][0] ) ? $block['meta']['block_size_partpage'][0] : 0;
      $block_size_fullpage = isset( $block['meta']['block_size_fullpage'][0] ) ? $block['meta']['block_size_fullpage'][0] : 0;
      $block_custom_size = isset( $block['meta']['block_custom_size'][0] ) ? $block['meta']['block_custom_size'][0] : 0;
      $block_meta_title = isset( $block['meta']['block_title'][0] ) ? $block['meta']['block_title'][0] : '';

      $post_title = $block['post_title'];
      $authors_name = $block['authors_name'];
      $word_count = $block['word_count'];
      $photo_count = $block['photo_count'];
      $is_finished = $block['is_finished'];
      $due_date_standing = $block['due_date_standing'];
      $due_date_format = $block['due_date_format'];
      $block_status = $block['status'];

      $assignToUsers = $block['authors'];
      $metaDueDate = $block['meta']['due_date'][0];
    ?>
    <tr class="tr-article-block edit-tr-block edit-tr-block-<?php echo $post_id;?> <?php echo ($isCover > 0) ? 'ui-is-covered' : 'ui-not-cover-page';?> " data-block-id="<?php echo $post_id;?>">
      <input type="hidden" name="task[blocks][block_id][]" value="<?php echo $post_id;?>">
      <input type="hidden" name="task[blocks][is_cover][]" value="<?php echo $isCover;?>">
      <td>
        <?php if ( $is_finished != 4 ) : ?>
          <input type="checkbox" class="bulk-edit-blocks status-<?php echo $is_finished;?>" name="bulk-edit[]" value="<?php echo $post_id;?>">
        <?php endif; ?>
        <span class="page-number-title">Page</span> : <span class="bock-page-number"><?php echo $pageNumber;?></span>
        <?php if ( $can_be_edited ) : ?>
          <div class="row-actions row-action-<?php echo $post_id;?>">
            <?php //if ( is_super_admin() || $is_finished != 4 ) : ?>
              <span class="edit"><a href="#" class="edit-block " data-block-id="<?php echo $post_id;?>" aria-label="">Edit</a></span>
            <?php //endif; ?>
            <?php //if ( $isCover != 1 && is_super_admin() || $is_finished != 4 ) : ?>
            | <span class="remove-block"><a href="<?php echo $deleteUrl;?>" class="remove-block" data-block-id=<?php echo $post_id;?>>Remove </a></span>
            <?php //endif; ?>
            <?php //if ( is_super_admin() || $is_finished != 4 ) : ?>
              | <span class="view-block"><a href="<?php echo $editUrl;?>&yearbook_id=<?php echo $yearbook_id;?>" class="view-block" data-block-id="<?php echo $post_id;?>"><?php echo $verbage_fields['view_blocks'];?> </a></span>
            <?php //endif; ?>
          </div>
        <?php endif; ?>
      </td>
      <td><span class="page-article-position"><?php echo $menu_order;?></span></td>
      <td>
        <span class="page-article-block_size"><?php echo $block_size;?></span>
      </td>
      <td><span class="page-article-title"><?php echo $post_title;?></span></td>
      <td><span class="page-article-author-name"><?php echo $authors_name;?></span></td>
      <td><span class="page-article-word-acount"><?php echo $word_count;?></span></td>
      <td><span class="page-article-photo-count"><?php echo $photo_count;?></span></td>
      <td>
        <?php if ( $is_finished > 0 ) : ?>
          <span class="page-article-is-finished">Done</span>
        <?php else: ?>
          <span class="page-article-due-date-standing"><?php echo $due_date_standing;?></span> | <span class="page-article-due-date-format"><?php echo $due_date_format; ?></span>
        <?php endif; ?>
        <?php if( $is_finished == 4 && ( yb_check_user_role(['administrator','editor']) || is_super_admin() ) ) : ?>
          <a href="<?php echo admin_url('admin.php?page=YearBook&_method=previewArticle&article-id='.$post_id);?>" title="Preview" target="_blank"><span class="dashicons dashicons-visibility"></span></a>
        <?php endif; ?>
      </td>
      <td class="page-status">
        <p class="status-ajax-msg-<?php echo $post_id;?> yb-status"></p>
        <?php if ( $can_be_edited ) : ?>
        <select class="form-control form-control-sm status-project" name="block_status" data-block-id="<?php echo $post_id;?>">
          <?php foreach( $page_status as $keyStatus => $valStatus ) : ?>
            <option value="<?php echo $keyStatus;?>" <?php echo ( $is_finished == $keyStatus ) ? 'selected':'';?>><?php echo $valStatus;?></option>
          <?php endforeach; ?>
        </select>
        <?php endif; ?>
      </td>
    </tr>
    <!-- start Edit block -->
    <tr class="tr-article-block-edit edit-tr edit-tr-<?php echo $post_id;?>">
      <td colspan="9" class="tr-article-block-edit-td">
        <div class="col-md-12 edit-div-col-container">
          <div class="edit-div-<?php echo $post_id;?> edit-div-container">
            <div class="row row-blocks update-block-container" data-block-id="<?php echo $post_id;?>">
              <input type="hidden" name="block_yearbook_id" value="<?php echo $yearbook_id;?>" class="block-parent-id">
              <input type="hidden" name="yearbook_status" value="<?php echo $post_status;?>" class="yearbook-status">
              <input type="hidden" name="yearbook_school_admin_id" value="<?php echo $school_admin_id;?>" class="yearbook_school_admin_id">

              <input type="hidden" name="meta_bock_title" value="<?php echo $block_meta_title;?>" class="meta_block_title">
              <input type="hidden" name="meta_block_status" value="<?php echo $block_status;?>" class="meta_block_status">
              <input type="hidden" name="meta_block_status_finish" value="<?php echo $is_finished;?>" class="meta_block_status_finish">

              <!--<div class="form-groupx xcol-md-6">-->
              <div class="form-group col-md-3 size-order-container">
                <!-- <label for="NameInput"><?php //echo $block_size_fullpage;;?></label> -->
                <div class="row edit-div-row">
                  <div class="col-md-12 edit-div-row-col">
                    <div class="form-row edit-div-form-row">
                      <div class="form-group col-md-6 size_fullpage-container">
                        <label for="block-size_fullpage" class="label-block-size_fullpage"><?php echo $verbage_fields['block_size_fullpage'];?></label>
                        <select class="form-control form-control-sm block-size_fullpage" name="block_size_fullpage">
                          <?php foreach( $templatePageFullSizes as $key => $size ) : ?>
                            <option value="<?php echo $key;?>" <?php echo ($block_size_fullpage == $key) ? 'selected':'';?>><?php echo $size;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group col-md-6 size_partpage-container">
                        <label for="block-size_partpage" class="label-block-size_partpage"><?php echo $verbage_fields['block_size_partpage'];?></label>
                        <select class="form-control form-control-sm block-size_partpage" name="block_size_partpage">
                          <?php foreach( $templatePagePartSizes as $key => $size ) : ?>
                            <option value="<?php echo $key;?>" <?php echo ($block_part_size == $key) ? 'selected':'';?>><?php echo $size;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 custom-page-size-form-container">
                    <div class="custom-page-size-container" style="<?php echo ( $block_size_fullpage != '-1' ) ? 'display:none;' : ''; ?>">
                      <label for="custom-block-size" class="custom-block-size-label">Custom Size</label>
                      <input type="text" class="form-control form-control-sm custom-block-size" name="custom_block_size_fullpage" autocomplete="off" value="<?php echo ( $block_size_fullpage != '-1' ) ? 0 : $block_size; ?>">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-6 menu-order-form-container">
                    <div class="menu-order-container">
                      <?php
                        if ( $isCover == 1 ) {
                          $menu_order = 0;
                        }
                      ?>
                        <label for="block-menu_order" class="block-menu_order-label">Article Position</label>
                        <input class="form-control form-control-sm block-menu_order" name="menu_order" type="text" value="<?php echo $menu_order;?>">
                    </div>
                  </div>
                  <div class="form-group col-md-6 is-cover-container">
                    <?php if ( is_super_admin() ) : ?>
                      <label for="block-is_cover" class="is-cover-label">Is Cover?</label>
                      <input class="form-checkbox block-is_cover" name="is_cover" type="checkbox" value="1" <?php echo ($isCover == 1) ? 'checked':'';?> >
                    <?php else: ?>
                      <input class="block-cover block-is_cover" name="is_cover" type="hidden" value="<?php echo $isCover;?>" >
                    <?php endif; ?>
                  </div>
                </div>

              </div>
              <div class="form-group col-md-3 article-name-container">
                <label for="NameInput" class="article-title-label"><?php echo $verbage_fields['block_title'];?></label>
                <div class="row article-title-container">
                  <input type="text" class="form-control form-control-sm block-title" name="block_title" autocomplete="off" value="<?php echo $post_title;?>">
                </div>
              </div>
              <!--</div>-->

              <!--<div class="form-groupx xcol-md-6">-->
              <div class="form-group col-md-3 assign_to-container">
                <div>
                  <label for="NameInput" class="assign-to-label"><?php echo $verbage_fields['assign_to'];?></label></div>

                  <select class="assign_to" id="assign_to-<?php echo $post_id;?>" name="post_author[]" multiple data-selected-text-format="count > 2" data-live-search="true" data-actions-box="true">
                    <?php if ( $contributors ) : ?>
                      <?php foreach( $contributors as $key => $assign_to ) : ?>
                        <option value="<?php echo $assign_to->ID;?>" <?php echo ( in_array($assign_to->ID, $assignToUsers) ) ? 'selected': '';?> >
                          <?php echo $assign_to->display_name;?>
                        </option>
                      <?php endforeach; ?>
                    <?php endif; ?>

                  </select>
                  <input type="hidden" name="assign_to_input" value="" class="assign_to_input">
                </div>
                <div class="form-group col-md-2 due_date-container">
                  <label for="NameInput" class="due_date-label"><?php echo $verbage_fields['due_date'];?></label>
                  <input type="text" class="form-control form-control-sm due_date" name="due_date" value="<?php echo $metaDueDate;?>" autocomplete="off">
                </div>
                <!--</div>-->

                <div class="form-group col-md-12">
                  <div class="alert alert-primary update-block-ajax-msg" role="alert"></div>
                  <div class="btn-action">
                    <?php if ( $is_finished == 0 ) : ?>
                      <a href="#" data-post-id="<?php echo $post_id;?>" class="btn-sm btn-primary task-notify ">Send Notification</a>
                    <?php endif; ?>
                    <a href="#" class="update-block btn-sm btn-primary new-block-btn" data-block-id="<?php echo $post_id;?>">Update</a>
                    <a href="#" class="cancel-update-block btn-sm btn-primary new-block-btn">Cancel</a>
                    <?php if ( is_super_admin() ) : ?>
                      <a href="#" class="btn-sm btn-danger clear-reminder-block" data-block-id="<?php echo $post_id;?>">Clear Reminder</a>
                    <?php endif; ?>

                  </div>
                  <div class="add-article">
                    <div class="alert alert-primary add-block-ajax-msg" role="alert"></div>
                    <a href="#" class="save_block btn-sm btn-primary new-block-btn">Save</a>
                    <a href="#" class="cancel-save-block btn-sm btn-primary new-block-btn">Cancel</a>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </td>
      </tr>
    <!-- end Edit block -->
  <?php endforeach; ?>
<?php endif; ?>
