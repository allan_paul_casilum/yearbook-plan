<?php if(count($images) > 0) { ?>
        <?php foreach($images as $k => $v) { ?>
                <div class="card image-<?php echo $v->ID;?>">
                <?php if( wp_attachment_is_image($v->ID) ) : ?>
                        <?php echo wp_get_attachment_image( $v->ID, "medium", "", array( "class" => "card-img-top" ) );  ?>
                <?php else : ?>
                        <p class="card-text"><?php echo basename( get_attached_file($v->ID) );?></p>
                <?php endif; ?>
                  <a href="#" class="btn btn-danger btn-sm remove-img-db" data-id="<?php echo $v->ID;?>">Remove</a>
                </div>
        <?php } ?>
<?php } ?>
