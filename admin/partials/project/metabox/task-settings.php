<div class="bootstrap-iso">
  <?php //echo $post_yearbook_id; ?>
  <?php $yearbook_id = $post_yearbook_id; ?>
  <p><a href="<?php echo admin_url('admin.php?page=YearBook&_method=showYearbook&id='.$yearbook_id.'');?>">Back To YearBook List</a></p>
  <p>Word Count: <?php echo $word_count;?></p>
  <p>Photo Count: <?php echo $photo_count;?></p>
  <p>Approximate Page Size: <?php echo yb_get_approximate_page_size(['current_word_count' => $word_count, 'current_photo_count' => $photo_count]);?></p>
  <p>Done?</p>
  <select name="done">
    <option value="0" <?php echo ($is_finished == 0) ? 'selected':'';?>>On Going</option>
    <option value="1" <?php echo ($is_finished == 1) ? 'selected':'';?>>Author Complete</option>
    <option value="5" <?php echo ($is_finished == 5) ? 'selected':'';?>>Checked</option>
    <option value="2" <?php echo ($is_finished == 2) ? 'selected':'';?>>Proof Read</option>
    <option value="3" <?php echo ($is_finished == 3) ? 'selected':'';?>>Ready for production</option>
    <option value="4" <?php echo ($is_finished == 4) ? 'selected':'';?>>In Production</option>
  </select>
  <input type="hidden" name="yearbook_id" value="<?php echo $yearbook_id;?>">
</div>
