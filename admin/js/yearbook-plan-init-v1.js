var last_block_assign_to = [];
var _data_assign_to = [];

function sort_block() {
	var _children = jQuery(".sortable");
	_children.sortable({
		items: "tr:not(.ui-is-covered)",
		placeholder: "ui-state-highlight",
		update: function( event, ui ) {
			// toggle_loader(true);

			var form = jQuery('#yearbook-plan-form').serialize();
			var _parent_id = jQuery('#yearbook_id').val();

			var data = {
				'action': 'yb_sort_blocks',
				'form': form
			};

			var xmlRequest = jQuery.ajax({
				url: ajaxurl,
				method: "POST",
				data: data
			});

			xmlRequest.done(function(res){
				get_data(_parent_id);
			});
		}
	});
	_children.disableSelection();
}

function update_block_ajax_msg(msg = '', show = false){
	var _div = jQuery('.update-block-ajax-msg');

	_div.hide();
	_div.html('');
	if(show){
		_div.show();
		_div.html(msg);
	}
}

function toggle_add_block(_show = true) {
	var add_block = jQuery('.add-block');
	if( _show ) {
		add_block.show();
	} else {
		add_block.hide();
	}
}

function toggle_move_button(_show = true) {
	var _move_btn = jQuery('.dashicons-move');
	if( _show ) {
		_move_btn.show();
	} else {
		_move_btn.hide();
	}
}

function toggle_btn(_show = true) {
	var _btn = jQuery(document).find('.btn');
	if(_show) {
		_btn.show();
	}else{
		_btn.hide();
	}
}

function toggle_update_button(_show = true) {
	var _btn = jQuery('#publish');
	if(_show) {
		_btn.show();
	}else{
		_btn.hide();
	}
}

function toggle_add_section(_show = true) {
	var add_section = jQuery('.add-page');
	if(_show) {
		add_section.show();
	}else{
		add_section.hide();
	}
}

function submit_form_data() {

	var form = jQuery('#yearbook-plan-form').serialize();

	var data = {
		'action': 'yb_create_yearbook',
		'form': form
	};

	var xmlRequest = jQuery.ajax({
		url: ajaxurl,
		method: "POST",
		data: data
	});
	return xmlRequest;
}

function get_data( _id, scroll_to = '' ) {

	toggle_loader(true);
	toggle_btn(false);

	var ajax_data = {
		'action'			: 'yb_get_yearbook',
		'yearbook_id' : _id
	};

	var xmlRequest = jQuery.ajax({
		url			: ajaxurl,
		method		: "POST",
		data		: ajax_data
	});

	jQuery('.content-placeholder').html('');

	xmlRequest.done(function(res) {
		jQuery('.content-placeholder').html(res);
		toggle_loader();
		ybSctollTo(scroll_to);
		toggle_btn(true);
		sort_block();
	});
}

function initDatePicker($sel) {
	jQuery($sel).find('.due_date').datepicker({
		dateFormat: 'yy-mm-dd'
	});
}

function toggle_section_ajax_msg(section_id = 0, msg = '', show = false) {
	var _msg = jQuery('.ajax-msg');
	var _msg_html = jQuery('.ajax-msg-alert-'+section_id);

	_msg.hide();

	if(show){
		_msg.show();
		_msg_html.show();
		_msg_html.html(msg);
	}
}

function btn_publish(show = false) {
	var btn_publish = jQuery('#publish');
	if(show) {
		btn_publish.show();
	}else{
		btn_publish.hide();
	}

}

function toggle_loader(show = false) {
	var _loader = jQuery('#loader');
	var bg = jQuery('.allteams-sendtask');

	if(show) {
			bg.css('opacity','0.5');
			_loader.show();
	}else{
		bg.css('opacity','1');
		_loader.hide();
	}

}


//added on 02/10/2020

function ybSctollTo( $sel = '' ) {

	if ( $sel == '' ) {
		var position = jQuery(document).height();
	} else {
		var position = jQuery($sel).position().top += 100;
	}
	jQuery('html, body').stop().animate({
      scrollTop: position
  }, 0);
}

function ybCustomFullSize() {
	jQuery(document).on('change', '.block-size_fullpage', function(e){
		var _this = jQuery(this);
		if ( _this.val() == '-1' ) {
			jQuery('.custom-page-size-container').show();
			ybAllowNumbericDecimalOnly();
		} else {
			jQuery('.custom-page-size-container').hide();
		}
	});
}

function ybShowHideElement($el, show = false) {
	var thisEl = jQuery($el);
	if(show) {
		thisEl.show();
	}else{
		thisEl.hide();
	}
}

function showBulkEditContainer( show = false ) {
	var bulkEdit = jQuery('.bulk-edit-container');
	if(show) {
		bulkEdit.show();
	}else{
		bulkEdit.hide();
	}
}

function initSelectPicker($sel) {
	var options;
}

function ybOrderAllowNumbericOnly(){
	jQuery(document).on('input', '.block-menu_order', function(evt){
		var self = jQuery(this);
		self.val(self.val().replace(/\D/g, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}
	});
}
function ybAllowNumbericDecimalOnly(){
	jQuery(document).on('input', '.custom-block-size', function(evt){
		var self = jQuery(this);
		self.val(self.val().replace(/[^0-9\.]/g, ''));
		if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}
	});
}

function _cancelUpdateBlock() {

	jQuery(document).on('click', '.cancel-update-block', function(e){
		e.preventDefault();
		var _edit_tr = jQuery('.edit-tr');
		jQuery(_edit_tr).find('.assign_to').selectpicker('destroy');
		_edit_tr.hide();

		ybShowHideElement('.row-actions', true);

		toggle_btn(true);
		btn_publish(true);
		toggle_move_button(true);
		toggle_update_button(true);
	});

}

var TaskNotify = function() {

	function init()
	{
		jQuery(document).on('click', '.task-notify', function(e) {
			e.preventDefault();

			var task_id = jQuery(this).data('post-id');
			var yearbook_id = jQuery('#yearbook_id');

			jQuery('.btn-action').hide();

			update_block_ajax_msg('Sending Notification', task_id, true);

			var data = {
				'action': 'task_notify',
				'post_id': task_id,
				'yearbook_id': yearbook_id.val(),
				'notify_mode': 'manual',
			};

			var ajaxSendNotificationTask = jQuery.ajax({
				url: ajaxurl,
				data: data,
				method: "POST",
			});

			ajaxSendNotificationTask.done(function(data){
				jQuery('.update-block-ajax-msg').html('Sent Notification');
				setTimeout(function(){
					jQuery('.update-block-ajax-msg').hide();
					jQuery('.btn-action').show();
				}, 3000);
			});

		});
	}

	function removeNotify() {
		jQuery(document).on('click', '.clear-reminder-block', function(e){
			e.preventDefault();
			var _this = jQuery(this);
			var _block_id = _this.data('block-id');

			jQuery('.btn-action').hide();

			update_block_ajax_msg('Deleting Reminder please wait...', true);

			var data = {
				'action': 'yb_clear_reminder',
				'post_id': _block_id,
			};

			var ajaxSendNotificationTask = jQuery.ajax({
				url: ajaxurl,
				data: data,
				method: "POST",
				dataType: "JSON"
			});

			ajaxSendNotificationTask.done(function(data){
				jQuery('.update-block-ajax-msg').html(data.msg);

				setTimeout(function(){
					jQuery('.update-block-ajax-msg').hide();
					jQuery('.btn-action').show();
				}, 3000);

			});

		});
	}

	return {
		init: function() {
			init();
			removeNotify();
		}
	};

}();

var EditArticle = function() {

	function update() {
		jQuery(document).on('click', '.update-block', function(e) {
			e.preventDefault();

			var _block_id = jQuery(this).data('block-id');
			var _td_parent = '.edit-tr-block-' + _block_id;

			var _parent_block = jQuery('.edit-div-'+_block_id);
			var _post_status = jQuery('#status').val();

			var _block_size_full_page = _parent_block.find('.block-size_fullpage').val();
			var _block_size_part_page = _parent_block.find('.block-size_partpage').val();
			var _block_size = _block_size_full_page + '' + _block_size_part_page ;
			var _block_title = _parent_block.find('.block-title').val();
			var _menu_order = _parent_block.find('.block-menu_order').val();
			var _due_date = _parent_block.find('.due_date').val();
			var _parent_id = jQuery('#yearbook_id').val();
			var _assign_to = _parent_block.find('#assign_to-'+_block_id).val();
			var custom_block_size_fullpage = _parent_block.find('.custom-block-size').val();
			var _meta_block_title = _parent_block.find('.meta_block_title').val();
			var _meta_block_status = _parent_block.find('.meta_block_status_finish').val();

			var _is_cover_val = 0;
			if ( _parent_block.find('.block-is_cover').is(':checked') ) {
				_is_cover_val = 1;
			}

			update_block_ajax_msg('Updating Block please Wait', _block_id, true);
			//toggle_btn(false);
			toggle_loader(true);

			jQuery('.btn-action').hide();

			var update_ajax_data = {
				'action': 'yb_update_yearbook',
				'yearbook_id' : _block_id,
				'assign_to' : _assign_to,
				'block_size_fullpage' : _block_size_full_page,
				'block_size_partpage' : _block_size_part_page,
				'block_size' : _block_size,
				'block_title' : _block_title,
				'meta_block_title' : _meta_block_title,
				'meta_block_status' : _meta_block_status,
				'is_cover' : _is_cover_val,
				'menu_order' : _menu_order,
				'custom_block_size_fullpage' : custom_block_size_fullpage,
				'due_date' : _due_date,
				'post_status' : _post_status,
				'parent_id' : _parent_id,
			};
			//console.log(update_ajax_data);

			var xmlRequest = jQuery.ajax({
				url: ajaxurl,
				method: "POST",
				data: update_ajax_data
			});

			xmlRequest.done(function(res){
				toggle_loader();
				get_data(_parent_id, _td_parent);
				toggle_btn(true);
				btn_publish(true);
				toggle_move_button(true);
				toggle_update_button(true);
			});

		});
	}

	function edit() {
		jQuery(document).on('click', '.edit-block', function(e){
			e.preventDefault();
			var _edit_tr = jQuery('.edit-tr');
			var _this = jQuery(this);
			var _edit_current_block = '.edit-tr-' + _this.data('block-id');
			var _edit_block_div = '.edit-div-' + _this.data('block-id');
			_edit_tr.hide();

			ybShowHideElement('.add-block');
			ybShowHideElement('.add-article');
			ybShowHideElement('.row-actions');

			ybCustomFullSize();

			update_block_ajax_msg('');
			initDatePicker(_edit_current_block);
			_cancelUpdateBlock();

			jQuery(_edit_current_block).find('.assign_to').selectpicker();
			jQuery(_edit_current_block).show();

		});
	}

	return {
		init: function(){
			edit();
			update();
		}
	};
}();

var UpdateArticleStatus = function() {
	return {
		init : function() {
			jQuery(document).on('change', '.status-project', function() {
				var _this = jQuery(this);
				var _block_id = _this.data('block-id');
				var _ajax_msg = jQuery('.status-ajax-msg-' + _block_id);

				var _block_id = jQuery(this).data('block-id');
				var _td_parent = '.edit-tr-block-' + _block_id;

				var ajax_data = {
					'action': 'yb_change_status',
					'block_id' : _block_id,
					'status' : _this.val()
				};

				var xmlRequest = jQuery.ajax({
					url: ajaxurl,
					method: "POST",
					data: ajax_data
				});
				//_ajax_msg.html('Updating Status please wait.');
				toggle_loader(true);
				xmlRequest.done(function(res){
					//_ajax_msg.html('Update successfully.');
					_ajax_msg.html('');
					var yearbook_id = jQuery('#yearbook_id');
					toggle_loader();
					get_data( yearbook_id.val() , _td_parent );
					toggle_btn(true);
					btn_publish(true);
					toggle_move_button(true);
					toggle_update_button(true);
				});

			});
		}
	};
}();

var AddArticle = function() {

	function _submit_form_data( $sel ) {

		var form = jQuery($sel).serialize();

		var data = {
			'action': 'yb_add_new_article',
			'form': form
		};

		var xmlRequest = jQuery.ajax({
			url: ajaxurl,
			method: "POST",
			data: data
		});
		return xmlRequest;
	}

	function cancelBtn() {
		jQuery(document).on('click', '.cancel-save-block', function(e) {
			e.preventDefault();
			//jQuery('.block-action').find('.row-blocks').remove();
			jQuery('.yb-add-container').find('.block-action').html('');

			ybShowHideElement('.row-actions', true);
			btn_publish(true);
			toggle_move_button(true);
			toggle_update_button(true);
			ybShowHideElement('.add-block', true);
			ybShowHideElement('.btn-action', true);
			ybShowHideElement('.add-article');
		});
	}

	function addBtn() {
		jQuery(document).on('click', '.add-block', function(){
				var last_block = jQuery('.content-placeholder .edit-tr').last().find('.edit-div-container');
				var _block_id = last_block.data('block-id');
				var _edit_div = '.edit-div-' + _block_id;
				var last_block_full_page_sel = last_block.find('.block-size_fullpage').val();
				var last_block_part_page_sel = last_block.find('.block-size_partpage').val();
				var last_block_due_date = last_block.find('.due_date').val();
				var last_block_assign_to = last_block.find('.assign_to .selectpicker').val();
				var last_block_menu_order = last_block.find('.block-menu_order').val();
				var last_block_is_cover = last_block.find('.block-is_cover').val();
				var last_select_contributors = last_block.find('.assign_to');

				last_block.find('.assign_to').selectpicker('destroy');
				last_block.find('.due_date').datepicker('destroy');

				jQuery('.block-action').append( '<form id="add-new-block">' + last_block.html() + '</form>' );
				jQuery('.block-action').find('.assign_to option:selected').prop("selected", false);
				jQuery('.block-action').find('.assign_to').selectpicker();
				jQuery('.block-action').find('.block-title').val('');
				jQuery('.block-action').find('.block-is_cover').prop('checked', false);
				jQuery('.block-action').find('.menu-order-form-container').hide();

				jQuery('.block-action').find('.due_date').removeAttr('id');
				jQuery('.block-action').find('.due_date').datepicker({
					dateFormat: 'yy-mm-dd'
				});

				ybShowHideElement('.row-actions');
				ybCustomFullSize();
				//hide button
				jQuery(this).hide();

				btn_publish(false);
				ybShowHideElement('.edit-tr');
				ybShowHideElement('.btn-action');
				ybShowHideElement('.add-article', true);
				ybShowHideElement('.update-block-ajax-msg');
				ybShowHideElement('.add-block-ajax-msg');
				toggle_move_button(false);
				toggle_update_button(false);
		});
	}

	function save() {
		jQuery(document).on('click', '.save_block', function(e){
			e.preventDefault();
			jQuery('.add-block-ajax-msg').show();
			jQuery('.add-block-ajax-msg').html('Adding Block Please Wait');
			jQuery('.new-block-btn').hide();
			var block_yearbook_id = jQuery('#yearbook_id');
			_submit_form_data('#add-new-block').done(function(res){

				// console.log(res);
				// console.log(block_yearbook_id.val());

				get_data(block_yearbook_id.val());

				jQuery('.block-action').html('');

				ybShowHideElement('.row-actions', true);
				btn_publish(true);
				toggle_move_button(true);
				toggle_update_button(true);
				ybShowHideElement('.add-block', true);
				ybShowHideElement('.btn-action', true);
				ybShowHideElement('.add-article');
			});
		});//.save block
	}

	return {
		init : function() {
			addBtn();
			cancelBtn();
			save();
		}
	};
}();

jQuery(function () {
	toggle_loader();
	EditArticle.init();
	AddArticle.init();
	UpdateArticleStatus.init();
	TaskNotify.init();
	sort_block();
	ybAllowNumbericDecimalOnly();
	ybOrderAllowNumbericOnly();
});
