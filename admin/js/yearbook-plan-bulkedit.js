
function bulk_ajax_msg(msg = '', show = false){
	var _div = jQuery('.ajax-msg');

	_div.hide();
	_div.html('');
	if(show){
		_div.show();
		_div.html(msg);
	}
}

var BulkEdit = function() {
	return {
		init: function(){
			var _bulk_edit_btn = jQuery('.bulk-edit-btn');

			function initBulkEdit()
			{
				var _bulk_edit_val = jQuery("input[name='bulk-edit[]']:checkbox:checked")
              .map(function(){return jQuery(this).val();}).get();
				var _yearbook_id = jQuery('#yearbook_id').val();
				var ajax_data = {
					'action': 'bulk_edit_init',
					'yearbook_id' : _yearbook_id,
					'edit_block_id' : _bulk_edit_val
				};

				var xmlRequest = jQuery.ajax({
					url: ajaxurl,
					method: "POST",
					data: ajax_data
				});

				return xmlRequest;
			}

			function initBulkUpdate()
			{
				var _bulk_edit_val = jQuery("input[name='bulk-edit-ids[]']")
              .map(function(){return jQuery(this).val();}).get();
				var _due_date = jQuery('.due_date').val();
				var _status = jQuery('.block_status').val();
				var _assign_to = jQuery('.assign_to').val();
				var _is_cover = jQuery('.bulk-edit-block-is_cover');

				var _is_cover_val = 0;
				if ( _is_cover.is(':checked') ) {
					_is_cover_val = 1;
				}

				var ajax_data = {
					'action': 'bulk_edit_udpate',
					'edit_block_ids' : _bulk_edit_val,
					'due_date' : _due_date,
					'status' : _status,
					'assign_to' : _assign_to,
					'is_cover' : _is_cover_val,
				};

				var xmlRequest = jQuery.ajax({
					url: ajaxurl,
					method: "POST",
					data: ajax_data
				});

				return xmlRequest;
			}

			jQuery('body').on('click', '.bulk-edit-btn', function(e){
				e.preventDefault();
				initBulkEdit().done(function(data){
					jQuery('.bulk-edit-container').html(data);
					jQuery('.due_date').datepicker({
						dateFormat: 'yy-mm-dd'
					});
				});
			});

			//bulk delete
			jQuery('body').on('click', '.bulk-delete-ajax', function(e){
				e.preventDefault();
				
				var _bulk_edit_val = jQuery("input[name='bulk-edit-ids[]']").map(function(){return jQuery(this).val();}).get();
				var _ajax_msg = '';
				var yearbook_id = jQuery('#yearbook_id').val();
				var school_id = jQuery('#school_admin').val();

				jQuery('.bulk-edit-container').find('.btn').hide();

				var ajax_data = {
					'action': 'bulk_delete',
					'block_ids' : _bulk_edit_val,
					'yearbook_id' : yearbook_id,
					'school_id' : school_id
				};

				console.log(ajax_data);

				var xmlRequest = jQuery.ajax({
					url: ajaxurl,
					method: "POST",
					data: ajax_data,
					dataType: "JSON"
				});

				var numberArticles = jQuery(".article-lists li").length;
				
				let text = "Are you sure you want to delete " + numberArticles + " Articles ? ";
				if (confirm(text) == true) {
					_ajax_msg = '<div class="alert alert-primary">Deleting articles please wait...</div>';
					bulk_ajax_msg(_ajax_msg, true);

					xmlRequest.done(function(data){
						_ajax_msg = '<div class="alert alert-primary">' + data.msg + '</div>';
						bulk_ajax_msg(_ajax_msg, true);

						setTimeout(function(){
							bulk_ajax_msg('');
							jQuery('.bulk-edit-container').find('.btn').show();
							jQuery(".article-lists li").hide();
						}, 3000);

						setTimeout(function(){
							var redirectUrl = rest_object.app_url + '/wp-admin/admin.php?page=YearBook&_method=showYearbook&id='+yearbook_id+'&school_id='+school_id;
							window.location.replace(redirectUrl);
						}, 1000);
					});
				} else {
					jQuery('.bulk-edit-container').find('.btn').show();
					//alert("You canceled!");
				}
				
			});

			//clear reminder bulk button
			jQuery('body').on('click', '.bulk-clear-reminder', function(e){
				e.preventDefault();
				var _bulk_edit_val = jQuery("input[name='bulk-edit-ids[]']")
              .map(function(){return jQuery(this).val();}).get();
				var _ajax_msg = '';

				jQuery('.bulk-edit-container').find('.btn').hide();

				_ajax_msg = '<div class="alert alert-primary">Deleting reminders please wait...</div>';
				bulk_ajax_msg(_ajax_msg, true);

				var ajax_data = {
					'action': 'bulk_clear_reminder',
					'block_ids' : _bulk_edit_val,
				};

				var xmlRequest = jQuery.ajax({
					url: ajaxurl,
					method: "POST",
					data: ajax_data,
					dataType: "JSON"
				});
				xmlRequest.done(function(data){
					_ajax_msg = '<div class="alert alert-primary">' + data.msg + '</div>';
					bulk_ajax_msg(_ajax_msg, true);
					setTimeout(function(){
						bulk_ajax_msg('');
						jQuery('.bulk-edit-container').find('.btn').show();
					}, 3000);
				});
			});

			//update bulk button
			jQuery('body').on('click', '.update-bulk-edit', function(e){
				e.preventDefault();
				var _yearbook_id = jQuery('#yearbook_id').val();
				initBulkUpdate().done(function(data){
					toggle_loader();

					jQuery('.bulk-edit-container').html('');

					get_data(_yearbook_id);

					toggle_btn(true);
					btn_publish(true);
					toggle_move_button(true);
					toggle_update_button(true);

				});
			});

			//cancel or close the bulk edit UI
			jQuery('body').on('click', '.cancel-bulk-edit', function(e){
				e.preventDefault();
				jQuery('.bulk-edit-container').html('');
			});

			jQuery("#checkAll").change(function (e) {
			    jQuery(".bulk-edit-blocks").not('.status-4').prop('checked', jQuery(this).prop("checked"));
			});

			//jQuery('.bulk-edit-btn').prop('disabled', true);
		}//init: function()
	};
}();

jQuery(function () {
	BulkEdit.init();
});
